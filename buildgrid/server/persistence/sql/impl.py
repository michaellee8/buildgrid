# Copyright (C) 2019 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from contextlib import contextmanager
import logging
import os
import select
from threading import Thread
import time
from datetime import datetime
from tempfile import NamedTemporaryFile
from itertools import chain, combinations, product
from typing import List, Dict, Tuple, Iterable

from alembic import command
from alembic.config import Config
from sqlalchemy import and_, create_engine, event, func, or_, text, union, literal_column
from sqlalchemy.orm.session import sessionmaker

from buildgrid._protos.google.longrunning import operations_pb2
from ...._enums import LeaseState, MetricCategories, OperationStage
from ....settings import MAX_JOB_BLOCK_TIME
from ....utils import JobState, hash_from_dict, convert_values_to_sorted_lists
from ..interface import DataStoreInterface
from .models import digest_to_string, Job, Lease, Operation

from buildgrid._exceptions import DatabaseError

Session = sessionmaker()


def sqlite_on_connect(conn, record):
    conn.execute("PRAGMA journal_mode=WAL")
    conn.execute("PRAGMA synchronous=NORMAL")


class SQLDataStore(DataStoreInterface):

    def __init__(self, storage, *, connection_string=None, automigrate=False,
                 connection_timeout=5, poll_interval=1, **kwargs):
        super().__init__()
        self.__logger = logging.getLogger(__name__)
        self.__logger.info("Creating SQL scheduler with: "
                           f"automigrate=[{automigrate}], connection_timeout=[{connection_timeout}] "
                           f"poll_interval=[{poll_interval}], kwargs=[{kwargs}]")

        self.storage = storage
        self.response_cache = {}
        self.connection_timeout = connection_timeout
        self.poll_interval = poll_interval
        self.watcher = Thread(name="JobWatcher", target=self.wait_for_job_updates, daemon=True)
        self.watcher_keep_running = True

        # Set-up temporary SQLite Database when connection string is not specified
        if not connection_string:
            tmpdbfile = NamedTemporaryFile(prefix='bgd-', suffix='.db')
            self._tmpdbfile = tmpdbfile  # Make sure to keep this tempfile for the lifetime of this object
            self.__logger.warn("No connection string specified for the DataStore, "
                               f"will use SQLite with tempfile: [{tmpdbfile.name}]")
            automigrate = True  # since this is a temporary database, we always need to create it
            connection_string = f"sqlite:///{tmpdbfile.name}"

        self._create_sqlalchemy_engine(connection_string, automigrate, connection_timeout, **kwargs)

        # Make a test query against the database to ensure the connection is valid
        with self.session(reraise=True) as session:
            session.query(Job).first()

        self.watcher.start()

        self.capabilities_cache = {}

    def _create_sqlalchemy_engine(self, connection_string, automigrate, connection_timeout, **kwargs):
        self.automigrate = automigrate

        # Disallow sqlite in-memory because multi-threaded access to it is
        # complex and potentially problematic at best
        # ref: https://docs.sqlalchemy.org/en/13/dialects/sqlite.html#threading-pooling-behavior
        if self._is_sqlite_inmemory_connection_string(connection_string):
            raise ValueError(
                f"Cannot use SQLite in-memory with BuildGrid (connection_string=[{connection_string}]). "
                "Use a file or leave the connection_string empty for a tempfile.")

        if connection_timeout is not None:
            if "connect_args" not in kwargs:
                kwargs["connect_args"] = {}
            if self._is_sqlite_connection_string(connection_string):
                kwargs["connect_args"]["timeout"] = connection_timeout
            else:
                kwargs["connect_args"]["connect_timeout"] = connection_timeout

        # Only pass the (known) kwargs that have been explicitly set by the user
        available_options = set(['pool_size', 'max_overflow', 'pool_timeout', 'connect_args'])
        kwargs_keys = set(kwargs.keys())
        if not kwargs_keys.issubset(available_options):
            unknown_options = kwargs_keys - available_options
            raise TypeError(f"Unknown keyword arguments: [{unknown_options}]")

        self.__logger.debug(f"SQLAlchemy additional kwargs: [{kwargs}]")

        self.engine = create_engine(connection_string, echo=False, **kwargs)
        Session.configure(bind=self.engine)

        if self.engine.dialect.name == "sqlite":
            event.listen(self.engine, "connect", sqlite_on_connect)

        if self.automigrate:
            self._create_or_migrate_db(connection_string)

    def _is_sqlite_connection_string(self, connection_string):
        if connection_string:
            return connection_string.startswith("sqlite")
        return False

    def _is_sqlite_inmemory_connection_string(self, full_connection_string):
        if self._is_sqlite_connection_string(full_connection_string):
            # Valid connection_strings for in-memory SQLite which we don't support could look like:
            # "sqlite:///file:memdb1?option=value&cache=shared&mode=memory",
            # "sqlite:///file:memdb1?mode=memory&cache=shared",
            # "sqlite:///file:memdb1?cache=shared&mode=memory",
            # "sqlite:///file::memory:?cache=shared",
            # "sqlite:///file::memory:",
            # "sqlite:///:memory:",
            # "sqlite:///",
            # "sqlite://"
            # ref: https://www.sqlite.org/inmemorydb.html
            # Note that a user can also specify drivers, so prefix could become 'sqlite+driver:///'
            connection_string = full_connection_string

            uri_split_index = connection_string.find("?")
            if uri_split_index != -1:
                connection_string = connection_string[0:uri_split_index]

            if connection_string.endswith((":memory:", ":///", "://")):
                return True
            elif uri_split_index != -1:
                opts = full_connection_string[uri_split_index + 1:].split("&")
                if "mode=memory" in opts:
                    return True

        return False

    def __repr__(self):
        return f"SQL data store interface for `{repr(self.engine.url)}`"

    def activate_monitoring(self):
        # Don't do anything. This function needs to exist but there's no
        # need to actually toggle monitoring in this implementation.
        pass

    def deactivate_monitoring(self):
        # Don't do anything. This function needs to exist but there's no
        # need to actually toggle monitoring in this implementation.
        pass

    def _create_or_migrate_db(self, connection_string):
        self.__logger.warn("Will attempt migration to latest version if needed.")

        config = Config()
        config.set_main_option("script_location", os.path.join(os.path.dirname(__file__), "alembic"))

        with self.engine.begin() as connection:
            config.attributes['connection'] = connection
            command.upgrade(config, "head")

    @contextmanager
    def session(self, *, sqlite_lock_immediately=False, reraise=False):
        # Try to obtain a session
        try:
            session = Session()
            if sqlite_lock_immediately and session.bind.name == "sqlite":
                session.execute("BEGIN IMMEDIATE")
        except Exception as e:
            self.__logger.error("Unable to obtain a database session.", exc_info=True)
            raise DatabaseError("Unable to obtain a database session.") from e

        # Yield the session and catch exceptions that occur while using it
        # to roll-back if needed
        try:
            yield session
            session.commit()
        except:
            session.rollback()
            self.__logger.error("Error committing database session. Rolling back.", exc_info=True)
            if reraise:
                raise
        finally:
            session.close()

    def _get_job(self, job_name, session, with_for_update=False):
        jobs = session.query(Job)
        if with_for_update:
            jobs = jobs.with_for_update()
        jobs = jobs.filter_by(name=job_name)
        return jobs.first()

    def _check_job_timeout(self, job_internal, *, max_execution_timeout=None):
        """ Do a lazy check of maximum allowed job timeouts when clients try to retrieve
            an existing job.
            Cancel the job and related operations/leases, if we detect they have
            exceeded timeouts on access.

            Returns the `buildgrid.server.Job` object, possibly updated with `cancelled=True`.
        """
        if job_internal and max_execution_timeout and job_internal.worker_start_timestamp_as_datetime:
            if job_internal.operation_stage == OperationStage.EXECUTING:
                executing_duration = datetime.utcnow() - job_internal.worker_start_timestamp_as_datetime
                if executing_duration.total_seconds() >= max_execution_timeout:
                    self.__logger.warning(f"Job=[{job_internal}] has been executing for "
                                          f"executing_duration=[{executing_duration}]. "
                                          f"max_execution_timeout=[{max_execution_timeout}] "
                                          "Cancelling.")
                    job_internal.cancel_all_operations(data_store=self)
                    self.__logger.info(f"Job=[{job_internal}] has been cancelled.")
        return job_internal

    def get_job_by_action(self, action_digest, *, max_execution_timeout=None):
        try:
            with self.session() as session:
                jobs = session.query(Job).filter_by(action_digest=digest_to_string(action_digest))
                jobs = jobs.filter(Job.stage != OperationStage.COMPLETED.value)
                job = jobs.first()
                if job:
                    internal_job = job.to_internal_job(self)
                    return self._check_job_timeout(internal_job, max_execution_timeout=max_execution_timeout)
        except DatabaseError:
            raise
        return None

    def get_job_by_name(self, name, *, max_execution_timeout=None):
        try:
            with self.session() as session:
                job = self._get_job(name, session)
                if job:
                    internal_job = job.to_internal_job(self)
                    return self._check_job_timeout(internal_job, max_execution_timeout=max_execution_timeout)
        except DatabaseError:
            raise
        return None

    def get_job_by_operation(self, operation_name, *, max_execution_timeout=None):
        with self.session() as session:
            operation = self._get_operation(operation_name, session)
            if not operation:
                return None
            job = operation.job
            if job:
                internal_job = job.to_internal_job(self)
                return self._check_job_timeout(internal_job, max_execution_timeout=max_execution_timeout)

    def get_all_jobs(self):
        with self.session() as session:
            jobs = session.query(Job).filter(Job.stage != OperationStage.COMPLETED.value)
            return [j.to_internal_job(self) for j in jobs]

    def get_jobs_by_stage(self, operation_stage):
        with self.session() as session:
            jobs = session.query(Job).filter(Job.stage == operation_stage.value)
            return [j.to_internal_job(self, no_result=True) for j in jobs]

    def create_job(self, job):
        with self.session() as session:
            if self._get_job(job.name, session) is None:
                # Convert requirements values to sorted lists to make them json-serializable
                platform_requirements = job.platform_requirements
                convert_values_to_sorted_lists(job.platform_requirements)
                # Serialize the requirements
                platform_requirements_hash = hash_from_dict(platform_requirements)

                session.add(Job(
                    name=job.name,
                    action_digest=digest_to_string(job.action_digest),
                    do_not_cache=job.do_not_cache,
                    priority=job.priority,
                    operations=[],
                    platform_requirements=platform_requirements_hash,
                    stage=job.operation_stage.value,
                    queued_timestamp=job.queued_timestamp_as_datetime,
                    queued_time_duration=job.queued_time_duration.seconds,
                    worker_start_timestamp=job.worker_start_timestamp_as_datetime,
                    worker_completed_timestamp=job.worker_completed_timestamp_as_datetime
                ))
                self.response_cache[job.name] = job.execute_response

    def queue_job(self, job_name):
        with self.session(sqlite_lock_immediately=True) as session:
            job = self._get_job(job_name, session, with_for_update=True)
            job.assigned = False

    def update_job(self, job_name, changes):
        if "result" in changes:
            changes["result"] = digest_to_string(changes["result"])
        if "action_digest" in changes:
            changes["action_digest"] = digest_to_string(changes["action_digest"])

        with self.session() as session:
            job = self._get_job(job_name, session)
            job.update(changes)
            if self.engine.dialect.name == "postgresql":
                conn = session.connection()
                conn.execute(f"NOTIFY job_updated, '{job_name}';")

    def delete_job(self, job_name):
        if job_name in self.response_cache:
            del self.response_cache[job_name]

    def wait_for_job_updates(self):
        self.__logger.info("Starting job watcher thread")
        if self.engine.dialect.name == "postgresql":
            self._listen_for_updates()
        else:
            self._poll_for_updates()

    def _listen_for_updates(self):
        def _listen_loop():
            try:
                conn = self.engine.connect()
                conn.execute(text("LISTEN job_updated;").execution_options(autocommit=True))
            except Exception as e:
                raise DatabaseError("Could not start listening to DB for job updates") from e

            while self.watcher_keep_running:
                # Wait until the connection is ready for reading. Timeout after 5 seconds
                # and try again if there was nothing to read. If the connection becomes
                # readable, collect the notifications it has received and handle them.
                #
                # See http://initd.org/psycopg/docs/advanced.html#async-notify
                if select.select([conn.connection], [], [], 5) == ([], [], []):
                    pass
                else:

                    try:
                        conn.connection.poll()
                    except Exception as e:
                        raise DatabaseError("Error while polling for job updates") from e

                    while conn.connection.notifies:
                        notify = conn.connection.notifies.pop()
                        with self.watched_jobs_lock:
                            spec = self.watched_jobs.get(notify.payload)
                            if spec is not None:
                                new_job = self.get_job_by_name(notify.payload)
                                new_state = JobState(new_job)
                                if spec.last_state != new_state:
                                    spec.last_state = new_state
                                    spec.event.notify_change()

        while self.watcher_keep_running:
            # Wait a few seconds if a database exception occurs and then try again
            # This could be a short disconnect
            try:
                _listen_loop()
            except DatabaseError as e:
                self.__logger.warning(f"JobWatcher encountered exception: [{e}];"
                                      f"Retrying in poll_interval=[{self.poll_interval}] seconds.")
                # Sleep for a bit so that we give enough time for the
                # database to potentially recover
                time.sleep(self.poll_interval)

    def _get_watched_jobs(self):
        with self.session() as sess:
            jobs = sess.query(Job).filter(
                Job.name.in_(self.watched_jobs)
            )
            return [job.to_internal_job(self) for job in jobs.all()]

    def _poll_for_updates(self):
        def _poll_loop():
            while self.watcher_keep_running:
                time.sleep(self.poll_interval)
                if self.watcher_keep_running:
                    with self.watched_jobs_lock:
                        if self.watcher_keep_running:
                            try:
                                watched_jobs = self._get_watched_jobs()
                            except Exception as e:
                                raise DatabaseError("Couldn't retrieve watched jobs from DB") from e

                            for new_job in watched_jobs:
                                if self.watcher_keep_running:
                                    spec = self.watched_jobs[new_job.name]
                                    new_state = JobState(new_job)
                                    if spec.last_state != new_state:
                                        spec.last_state = new_state
                                        spec.event.notify_change()

        while self.watcher_keep_running:
            # Wait a few seconds if a database exception occurs and then try again
            try:
                _poll_loop()
            except DatabaseError as e:
                self.__logger.warning(f"JobWatcher encountered exception: [{e}];"
                                      f"Retrying in poll_interval=[{self.poll_interval}] seconds.")
                # Sleep for a bit so that we give enough time for the
                # database to potentially recover
                time.sleep(self.poll_interval)

    def store_response(self, job):
        digest = self.storage.put_message(job.execute_response)
        self.update_job(job.name, {"result": digest})
        self.response_cache[job.name] = job.execute_response

    def _get_operation(self, operation_name, session):
        operations = session.query(Operation).filter_by(name=operation_name)
        return operations.first()

    def get_operations_by_stage(self, operation_stage):
        with self.session() as session:
            operations = session.query(Operation)
            operations = operations.filter(Operation.job.has(stage=operation_stage.value))
            operations = operations.all()
            # Return a set of job names here for now, to match the `MemoryDataStore`
            # implementation's behaviour
            return set(op.job.name for op in operations)

    def get_all_operations(self) -> List[operations_pb2.Operation]:
        with self.session() as session:
            operations = session.query(Operation)
            operations = operations.filter(~Operation.job.has(stage=OperationStage.COMPLETED.value))  # type: ignore
            return [operation.to_protobuf() for operation in operations]

    def create_operation(self, operation, job_name):
        with self.session() as session:
            session.add(Operation(
                name=operation.name,
                job_name=job_name
            ))

    def update_operation(self, operation_name, changes):
        with self.session() as session:
            operation = self._get_operation(operation_name, session)
            operation.update(changes)

    def delete_operation(self, operation_name):
        # Don't do anything. This function needs to exist but there's no
        # need to actually delete operations in this implementation.
        pass

    def get_leases_by_state(self, lease_state):
        with self.session() as session:
            leases = session.query(Lease).filter_by(state=lease_state.value)
            leases = leases.all()
            # `lease.job_name` is the same as `lease.id` for a Lease protobuf
            return set(lease.job_name for lease in leases)

    def get_metrics(self):

        def _get_query_leases_by_state(session, category):
            # Using func.count here to avoid generating a subquery in the WHERE
            # clause of the resulting query.
            # https://docs.sqlalchemy.org/en/13/orm/query.html#sqlalchemy.orm.query.Query.count
            query = session.query(literal_column(category).label("category"),
                                  Lease.state.label("bucket"),
                                  func.count(Lease.id).label("value"))
            query = query.group_by(Lease.state)
            return query

        def _cb_query_leases_by_state(leases_by_state):
            # The database only returns counts > 0, so fill in the gaps
            for state in LeaseState:
                if state.value not in leases_by_state:
                    leases_by_state[state.value] = 0
            return leases_by_state

        def _get_query_operations_by_stage(session, category):
            # Using func.count here to avoid generating a subquery in the WHERE
            # clause of the resulting query.
            # https://docs.sqlalchemy.org/en/13/orm/query.html#sqlalchemy.orm.query.Query.count
            query = session.query(literal_column(category).label("category"),
                                  Job.stage.label("bucket"),
                                  func.count(Operation.name).label("value"))
            query = query.join(Job)
            query = query.group_by(Job.stage)
            return query

        def _cb_query_operations_by_stage(operations_by_stage):
            # The database only returns counts > 0, so fill in the gaps
            for stage in OperationStage:
                if stage.value not in operations_by_stage:
                    operations_by_stage[stage.value] = 0
            return operations_by_stage

        def _get_query_jobs_by_stage(session, category):
            # Using func.count here to avoid generating a subquery in the WHERE
            # clause of the resulting query.
            # https://docs.sqlalchemy.org/en/13/orm/query.html#sqlalchemy.orm.query.Query.count
            query = session.query(literal_column(category).label("category"),
                                  Job.stage.label("bucket"),
                                  func.count(Job.name).label("value"))
            query = query.group_by(Job.stage)
            return query

        def _cb_query_jobs_by_stage(jobs_by_stage):
            # The database only returns counts > 0, so fill in the gaps
            for stage in OperationStage:
                if stage.value not in jobs_by_stage:
                    jobs_by_stage[stage.value] = 0
            return jobs_by_stage

        metrics = {}
        try:
            with self.session() as session:
                # metrics to gather: (category_name, function_returning_query, callback_function)
                metrics_to_gather = [(MetricCategories.LEASES.value, _get_query_leases_by_state,
                                      _cb_query_leases_by_state),
                                     (MetricCategories.OPERATIONS.value, _get_query_operations_by_stage,
                                      _cb_query_operations_by_stage),
                                     (MetricCategories.JOBS.value, _get_query_jobs_by_stage,
                                      _cb_query_jobs_by_stage)]

                union_query = union(*[query_fn(session, f"'{category}'")
                                      for category, query_fn, _ in metrics_to_gather])
                union_results = session.execute(union_query).fetchall()

                grouped_results = {category: {} for category, _, _ in union_results}
                for category, bucket, value in union_results:
                    grouped_results[category][bucket] = value

                for category, _, category_cb in metrics_to_gather:
                    metrics[category] = category_cb(grouped_results.setdefault(category, {}))
        except DatabaseError:
            self.__logger.warning("Unable to gather metrics due to a Database Error.")
            return {}

        return metrics

    def _create_lease(self, lease, session, job=None):
        if job is None:
            job = self._get_job(lease.id, session)
            job = job.to_internal_job(self)
        session.add(Lease(
            job_name=lease.id,
            state=lease.state,
            status=None,
            worker_name=job.worker_name
        ))

    def create_lease(self, lease):
        with self.session() as session:
            self._create_lease(lease, session)

    def update_lease(self, job_name, changes):
        with self.session() as session:
            job = self._get_job(job_name, session)
            lease = job.active_leases[0]
            lease.update(changes)

    def load_unfinished_jobs(self):
        with self.session() as session:
            jobs = session.query(Job)
            jobs = jobs.filter(Job.stage != OperationStage.COMPLETED.value)
            jobs = jobs.order_by(Job.priority)
            return [j.to_internal_job(self) for j in jobs.all()]

    def assign_lease_for_next_job(self, capabilities, callback, timeout=None):
        """Return a list of leases for the highest priority jobs that can be run by a worker.

        NOTE: Currently the list only ever has one or zero leases.

        Query the jobs table to find queued jobs which match the capabilities of
        a given worker, and return the one with the highest priority. Takes a
        dictionary of worker capabilities to compare with job requirements.

        :param capabilities: Dictionary of worker capabilities to compare
            with job requirements when finding a job.
        :type capabilities: dict
        :param callback: Function to run on the next runnable job, should return
            a list of leases.
        :type callback: function
        :param timeout: time to wait for new jobs, caps if longer
            than MAX_JOB_BLOCK_TIME.
        :type timeout: int
        :returns: List of leases

        """
        if not timeout:
            return self._assign_job_leases(capabilities, callback)

        # Cap the timeout if it's larger than MAX_JOB_BLOCK_TIME
        if timeout:
            timeout = min(timeout, MAX_JOB_BLOCK_TIME)

        start = time.time()
        while time.time() + self.connection_timeout + 1 - start < timeout:
            leases = self._assign_job_leases(capabilities, callback)
            if leases:
                return leases
            time.sleep(0.5)
        if self.connection_timeout > timeout:
            self.__logger.warning(
                "Not providing any leases to the worker because the database connection "
                "timeout is longer than the remaining time to handle the request. "
                "Increase the worker's timeout to solve this problem.")
        return []

    def flatten_capabilities(self, capabilities: Dict[str, List[str]]) -> List[Tuple[str, str]]:
        """ Flatten a capabilities dictionary, assuming all of its values are lists. E.g.

        {'OSFamily': ['Linux'], 'ISA': ['x86-32', 'x86-64']}

        becomes

        [('OSFamily', 'Linux'), ('ISA', 'x86-32'), ('ISA', 'x86-64')] """
        return [
            (name, value) for name, value_list in capabilities.items()
            for value in value_list
        ]

    def get_partial_capabilities(self, capabilities: Dict[str, List[str]]) -> Iterable[Dict[str, List[str]]]:
        """ Given a capabilities dictionary with all values as lists,
        yield all partial capabilities dictionaries. """
        CAPABILITIES_WARNING_THRESHOLD = 10

        caps_flat = self.flatten_capabilities(capabilities)

        if len(caps_flat) > CAPABILITIES_WARNING_THRESHOLD:
            self.__logger.warning(
                "A worker with a large capabilities dictionary has been connected. "
                f"Processing its capabilities may take a while. Capabilities: {capabilities}")

        # Using the itertools powerset recipe, construct the powerset of the tuples
        capabilities_powerset = chain.from_iterable(combinations(caps_flat, r) for r in range(len(caps_flat) + 1))
        for partial_capability_tuples in capabilities_powerset:
            partial_dict: Dict[str, List[str]] = {}

            for tup in partial_capability_tuples:
                partial_dict.setdefault(tup[0], []).append(tup[1])
            yield partial_dict

    def get_partial_capabilities_hashes(self, capabilities: Dict) -> List[str]:
        """ Given a list of configurations, obtain each partial configuration
        for each configuration, obtain the hash of each partial configuration,
        compile these into a list, and return the result. """
        # Convert requirements values to sorted lists to make them json-serializable
        convert_values_to_sorted_lists(capabilities)

        # Check to see if we've cached this value
        capabilities_digest = hash_from_dict(capabilities)
        try:
            return self.capabilities_cache[capabilities_digest]
        except KeyError:
            # On cache miss, expand the capabilities into each possible partial capabilities dictionary
            capabilities_list = []
            for partial_capability in self.get_partial_capabilities(capabilities):
                capabilities_list.append(hash_from_dict(partial_capability))

            self.capabilities_cache[capabilities_digest] = capabilities_list
            return capabilities_list

    def _assign_job_leases(self, capabilities, callback):
        # Hash the capabilities
        capabilities_config_hashes = self.get_partial_capabilities_hashes(capabilities)
        try:
            with self.session(sqlite_lock_immediately=True) as session:
                jobs = session.query(Job).with_for_update(skip_locked=True)
                jobs = jobs.filter(Job.stage == OperationStage.QUEUED.value)
                jobs = jobs.filter(Job.assigned != True)  # noqa
                jobs = jobs.filter(Job.platform_requirements.in_(capabilities_config_hashes))
                job = jobs.order_by(Job.priority).first()
                # This worker can take this job if it can handle all of its configurations
                if job:
                    internal_job = job.to_internal_job(self)
                    leases = callback(internal_job)
                    if leases:
                        job.assigned = True
                        job.worker_start_timestamp = internal_job.worker_start_timestamp_as_datetime
                    for lease in leases:
                        self._create_lease(lease, session, job=internal_job)
                    return leases

        except DatabaseError:
            self.__logger.warning("Will not assign any leases this time due to a Database Error.")

        return []
