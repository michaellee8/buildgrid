# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
S3Storage
==================

A storage provider that stores data in an Amazon S3 bucket.
"""

import io
import logging

import boto3
from botocore.exceptions import ClientError

from buildgrid._enums import CleanupFailure
from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Digest
from buildgrid.utils import Failure
from .storage_abc import StorageABC


class S3Storage(StorageABC):

    def __init__(self, bucket, page_size=1000, **kwargs):
        self.__logger = logging.getLogger(__name__)

        self._bucket_template = bucket
        self._page_size = page_size
        self._s3 = boto3.resource('s3', **kwargs)

    def _get_bucket_name(self, digest):
        try:
            return self._bucket_template.format(digest=digest)
        except IndexError as e:
            self.__logger.error(f"Could not calculate bucket name for digest=[{digest}]. This "
                                "is either a misconfiguration in the BuildGrid S3 bucket "
                                "configuration, or a badly formed request.")
            raise

    def _construct_key(self, digest):
        return digest.hash + '_' + str(digest.size_bytes)

    def _deconstruct_key(self, key):
        parts = key.split('_')
        size_bytes = int(parts[-1])
        # This isn't as simple as just "the first part of the split" because
        # the hash part of the key itself might contain an underscore.
        hash = '_'.join(parts[0:-1])
        return hash, size_bytes

    def _multi_delete_blobs(self, bucket_name, digests):
        bucket = self._s3.Bucket(bucket_name)
        response = bucket.delete_objects(Delete={'Objects': digests})
        return_failed = []
        failed_deletions = response.get('Errors', [])
        for failed_key in failed_deletions:
            hash, size_bytes = self._deconstruct_key(failed_key['Key'])
            if self.has_blob(Digest(hash=hash, size_bytes=size_bytes)):
                fail_type = CleanupFailure.FAILURE
            else:
                fail_type = CleanupFailure.MISSING
            return_failed.append(Failure(hash, fail_type))
        return return_failed

    def has_blob(self, digest):
        self.__logger.debug(f"Checking for blob: [{digest}]")
        try:
            self._s3.Object(self._get_bucket_name(digest.hash),
                            self._construct_key(digest)).load()
        except ClientError as e:
            if e.response['Error']['Code'] not in ['404', 'NoSuchKey']:
                raise
            return False
        return True

    def get_blob(self, digest):
        self.__logger.debug(f"Getting blob: [{digest}]")
        try:
            obj = self._s3.Object(self._get_bucket_name(digest.hash),
                                  self._construct_key(digest))
            return io.BytesIO(obj.get()['Body'].read())
        except ClientError as e:
            if e.response['Error']['Code'] not in ['404', 'NoSuchKey']:
                raise
            return None

    def delete_blob(self, digest):
        self.__logger.debug(f"Deleting blob: [{digest}]")
        try:
            self._s3.Object(self._get_bucket_name(digest.hash),
                            self._construct_key(digest)).delete()
        except ClientError as e:
            if e.response['Error']['Code'] not in ['404', 'NoSuchKey']:
                raise

    def bulk_delete(self, digests):
        self.__logger.debug(f"Deleting blobs: [{digests}]")
        buckets_to_digest_lists = {}
        failed_deletions = []
        for digest in digests:
            bucket = self._get_bucket_name(digest.hash)
            if bucket in buckets_to_digest_lists:
                buckets_to_digest_lists[bucket].append({'Key': self._construct_key(digest)})
            else:
                buckets_to_digest_lists[bucket] = [{'Key': self._construct_key(digest)}]
            if len(buckets_to_digest_lists[bucket]) >= self._page_size:
                # delete items for this bucket, hit page limit
                failed_deletions += self._multi_delete_blobs(bucket,
                                                             buckets_to_digest_lists.pop(bucket))
        # flush remaining items
        for bucket in buckets_to_digest_lists:
            failed_deletions += self._multi_delete_blobs(bucket, buckets_to_digest_lists[bucket])
        return failed_deletions

    def begin_write(self, _digest):
        # TODO use multipart API for large blobs?
        return io.BytesIO()

    def commit_write(self, digest, write_session):
        self.__logger.debug(f"Writing blob: [{digest}]")
        write_session.seek(0)
        self._s3.Bucket(self._get_bucket_name(digest.hash)) \
                .upload_fileobj(write_session,
                                self._construct_key(digest))
        write_session.close()

    def is_cleanup_enabled(self):
        return True
