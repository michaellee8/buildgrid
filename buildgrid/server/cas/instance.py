# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Storage Instances
=================
Instances of CAS and ByteStream
"""

import logging

from buildgrid._enums import MetricRecordDomain
from buildgrid._exceptions import InvalidArgumentError, NotFoundError, OutOfRangeError, PermissionDeniedError
from buildgrid._protos.google.bytestream import bytestream_pb2
from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2 as re_pb2
from buildgrid._protos.google.rpc import code_pb2, status_pb2
from buildgrid.server.metrics_utils import (
    timed_method,
    Counter,
    ExceptionCounter,
    Distribution)
from buildgrid.server.metrics_names import (
    CAS_EXCEPTION_COUNT_METRIC_NAME,
    CAS_DOWNLOADED_BYTES_METRIC_NAME,
    CAS_UPLOADED_BYTES_METRIC_NAME,
    CAS_FIND_MISSING_BLOBS_NUM_REQUESTED_METRIC_NAME,
    CAS_FIND_MISSING_BLOBS_NUM_MISSING_METRIC_NAME,
    CAS_FIND_MISSING_BLOBS_PERCENT_MISSING_METRIC_NAME,
    CAS_FIND_MISSING_BLOBS_TIME_METRIC_NAME,
    CAS_BATCH_UPDATE_BLOBS_TIME_METRIC_NAME,
    CAS_BATCH_READ_BLOBS_TIME_METRIC_NAME,
    CAS_GET_TREE_TIME_METRIC_NAME,
    CAS_BYTESTREAM_READ_TIME_METRIC_NAME,
    CAS_BYTESTREAM_WRITE_TIME_METRIC_NAME)
from buildgrid.settings import HASH, HASH_LENGTH, MAX_REQUEST_SIZE, MAX_REQUEST_COUNT
from buildgrid.utils import create_digest, get_hash_type, get_unique_objects_by_attribute


class ContentAddressableStorageInstance:

    def __init__(self, storage, read_only=False):
        self.__logger = logging.getLogger(__name__)

        self._instance_name = None

        self.__storage = storage

        self.__read_only = read_only

        self._monitoring_bus = None

        if self.__storage:
            # Some clients skip uploading a blob with size 0.
            # We pre-insert the empty blob to make sure that it is available.
            empty_digest = create_digest(b'')
            session = self.__storage.begin_write(empty_digest)
            self.__storage.commit_write(empty_digest, session)

    # --- Public API ---

    @property
    def instance_name(self):
        return self._instance_name

    @ExceptionCounter(CAS_EXCEPTION_COUNT_METRIC_NAME, metric_domain=MetricRecordDomain.CAS)
    def register_instance_with_server(self, instance_name, server):
        """Names and registers the CAS instance with a given server."""
        if self._instance_name is None:
            server.add_cas_instance(self, instance_name)

            self._instance_name = instance_name

        else:
            raise AssertionError("Instance already registered")

    def add_monitoring_bus(self, monitoring_bus):
        self._monitoring_bus = monitoring_bus

    def hash_type(self):
        return get_hash_type()

    def max_batch_total_size_bytes(self):
        return MAX_REQUEST_SIZE

    def symlink_absolute_path_strategy(self):
        # Currently this strategy is hardcoded into BuildGrid
        # With no setting to reference
        return re_pb2.SymlinkAbsolutePathStrategy.DISALLOWED

    @timed_method(CAS_FIND_MISSING_BLOBS_TIME_METRIC_NAME, MetricRecordDomain.CAS, instanced=True)
    @ExceptionCounter(CAS_EXCEPTION_COUNT_METRIC_NAME, metric_domain=MetricRecordDomain.CAS)
    def find_missing_blobs(self, blob_digests):
        storage = self.__storage
        blob_digests = list(get_unique_objects_by_attribute(blob_digests, "hash"))
        missing_blobs = storage.missing_blobs(blob_digests)

        num_blobs_in_request = len(blob_digests)
        if num_blobs_in_request > 0:
            num_blobs_missing = len(missing_blobs)
            percent_missing = float((num_blobs_missing / num_blobs_in_request) * 100)

            with Distribution(CAS_FIND_MISSING_BLOBS_NUM_REQUESTED_METRIC_NAME,
                              monitoring_bus=self._monitoring_bus, instance_name=self._instance_name,
                              metric_domain=MetricRecordDomain.CAS) as metric_num_requested:
                metric_num_requested.count = float(num_blobs_in_request)

            with Distribution(CAS_FIND_MISSING_BLOBS_NUM_MISSING_METRIC_NAME,
                              monitoring_bus=self._monitoring_bus, instance_name=self._instance_name,
                              metric_domain=MetricRecordDomain.CAS) as metric_num_missing:
                metric_num_missing.count = float(num_blobs_missing)

            with Distribution(CAS_FIND_MISSING_BLOBS_PERCENT_MISSING_METRIC_NAME,
                              monitoring_bus=self._monitoring_bus, instance_name=self._instance_name,
                              metric_domain=MetricRecordDomain.CAS) as metric_percent_missing:
                metric_percent_missing.count = percent_missing

        return re_pb2.FindMissingBlobsResponse(missing_blob_digests=missing_blobs)

    @timed_method(CAS_BATCH_UPDATE_BLOBS_TIME_METRIC_NAME, MetricRecordDomain.CAS, instanced=True)
    @ExceptionCounter(CAS_EXCEPTION_COUNT_METRIC_NAME, metric_domain=MetricRecordDomain.CAS)
    def batch_update_blobs(self, requests):
        if self.__read_only:
            raise PermissionDeniedError(f"CAS instance {self._instance_name} is read-only")

        storage = self.__storage
        store = []
        for request_proto in get_unique_objects_by_attribute(requests, "digest.hash"):
            store.append((request_proto.digest, request_proto.data))

        response = re_pb2.BatchUpdateBlobsResponse()
        statuses = storage.bulk_update_blobs(store)

        with Counter(metric_name=CAS_UPLOADED_BYTES_METRIC_NAME,
                     monitoring_bus=self._monitoring_bus,
                     instance_name=self._instance_name,
                     metric_domain=MetricRecordDomain.CAS) as bytes_counter:
            for (digest, _), status in zip(store, statuses):
                response_proto = response.responses.add()
                response_proto.digest.CopyFrom(digest)
                response_proto.status.CopyFrom(status)
                if response_proto.status.code == 0:
                    bytes_counter.increment(response_proto.digest.size_bytes)

        return response

    @timed_method(CAS_BATCH_READ_BLOBS_TIME_METRIC_NAME, MetricRecordDomain.CAS, instanced=True)
    @ExceptionCounter(CAS_EXCEPTION_COUNT_METRIC_NAME, metric_domain=MetricRecordDomain.CAS)
    def batch_read_blobs(self, digests):
        storage = self.__storage

        response = re_pb2.BatchReadBlobsResponse()

        max_batch_size = self.max_batch_total_size_bytes()

        # Only process unique digests
        good_digests = []
        bad_digests = []
        requested_bytes = 0
        for digest in get_unique_objects_by_attribute(digests, "hash"):
            if len(digest.hash) != HASH_LENGTH:
                bad_digests.append(digest)
            else:
                good_digests.append(digest)
                requested_bytes += digest.size_bytes

        if requested_bytes > max_batch_size:
            raise InvalidArgumentError('Combined total size of blobs exceeds '
                                       'server limit. '
                                       f'({requested_bytes} > {max_batch_size} [byte])')

        blobs_read = storage.bulk_read_blobs(good_digests)

        with Counter(metric_name=CAS_DOWNLOADED_BYTES_METRIC_NAME,
                     monitoring_bus=self._monitoring_bus,
                     instance_name=self._instance_name,
                     metric_domain=MetricRecordDomain.CAS) as bytes_counter:
            for digest in good_digests:
                response_proto = response.responses.add()
                response_proto.digest.CopyFrom(digest)

                if digest.hash in blobs_read and blobs_read[digest.hash] is not None:
                    response_proto.data = blobs_read[digest.hash].read()
                    status_code = code_pb2.OK
                    bytes_counter.increment(digest.size_bytes)
                else:
                    status_code = code_pb2.NOT_FOUND

                response_proto.status.CopyFrom(status_pb2.Status(code=status_code))

        for digest in bad_digests:
            response_proto = response.responses.add()
            response_proto.digest.CopyFrom(digest)
            status_code = code_pb2.INVALID_ARGUMENT
            response_proto.status.CopyFrom(status_pb2.Status(code=status_code))

        return response

    @timed_method(CAS_GET_TREE_TIME_METRIC_NAME, MetricRecordDomain.CAS, instanced=True)
    @ExceptionCounter(CAS_EXCEPTION_COUNT_METRIC_NAME, metric_domain=MetricRecordDomain.CAS)
    def get_tree(self, request):
        storage = self.__storage

        response = re_pb2.GetTreeResponse()
        page_size = request.page_size

        if not request.page_size:
            request.page_size = MAX_REQUEST_COUNT

        root_digest = request.root_digest
        page_size = request.page_size

        with Counter(metric_name=CAS_DOWNLOADED_BYTES_METRIC_NAME,
                     monitoring_bus=self._monitoring_bus,
                     instance_name=self._instance_name,
                     metric_domain=MetricRecordDomain.CAS) as bytes_counter:
            def __get_tree(node_digest):
                nonlocal response, page_size, request

                if not page_size:
                    page_size = request.page_size
                    yield response
                    response = re_pb2.GetTreeResponse()

                if response.ByteSize() >= (MAX_REQUEST_SIZE):
                    yield response
                    response = re_pb2.GetTreeResponse()

                directory_from_digest = storage.get_message(
                    node_digest, re_pb2.Directory)

                bytes_counter.increment(node_digest.size_bytes)

                page_size -= 1
                response.directories.extend([directory_from_digest])

                for directory in directory_from_digest.directories:
                    yield from __get_tree(directory.digest)

                yield response
                response = re_pb2.GetTreeResponse()

            yield from __get_tree(root_digest)


class ByteStreamInstance:

    BLOCK_SIZE = 1 * 1024 * 1024  # 1 MB block size

    def __init__(self, storage, read_only=False):
        self.__logger = logging.getLogger(__name__)

        self._instance_name = None

        self.__storage = storage

        self.__read_only = read_only

        self._monitoring_bus = None

        if self.__storage:
            # Some clients skip uploading a blob with size 0.
            # We pre-insert the empty blob to make sure that it is available.
            empty_digest = create_digest(b'')
            session = self.__storage.begin_write(empty_digest)
            self.__storage.commit_write(empty_digest, session)

    # --- Public API ---

    @property
    def instance_name(self):
        return self._instance_name

    @ExceptionCounter(CAS_EXCEPTION_COUNT_METRIC_NAME, metric_domain=MetricRecordDomain.CAS)
    def register_instance_with_server(self, instance_name, server):
        """Names and registers the byte-stream instance with a given server."""
        if self._instance_name is None:
            server.add_bytestream_instance(self, instance_name)

            self._instance_name = instance_name

        else:
            raise AssertionError("Instance already registered")

    def add_monitoring_bus(self, monitoring_bus):
        self._monitoring_bus = monitoring_bus

    @timed_method(CAS_BYTESTREAM_READ_TIME_METRIC_NAME, MetricRecordDomain.CAS, instanced=True)
    @ExceptionCounter(CAS_EXCEPTION_COUNT_METRIC_NAME, metric_domain=MetricRecordDomain.CAS)
    def read(self, digest_hash, digest_size, read_offset, read_limit):
        if len(digest_hash) != HASH_LENGTH or not digest_size.isdigit():
            raise InvalidArgumentError(f"Invalid digest [{digest_hash}/{digest_size}]")

        digest = re_pb2.Digest(hash=digest_hash, size_bytes=int(digest_size))

        # Check the given read offset and limit.
        if read_offset < 0 or read_offset > digest.size_bytes:
            raise OutOfRangeError("Read offset out of range")

        elif read_limit == 0:
            bytes_remaining = digest.size_bytes - read_offset

        elif read_limit > 0:
            bytes_remaining = read_limit

        else:
            raise InvalidArgumentError("Negative read_limit is invalid")

        # Read the blob from storage and send its contents to the client.
        result = self.__storage.get_blob(digest)
        if result is None:
            raise NotFoundError("Blob not found")

        elif result.seekable():
            result.seek(read_offset)

        else:
            result.read(read_offset)

        with Counter(metric_name=CAS_DOWNLOADED_BYTES_METRIC_NAME,
                     monitoring_bus=self._monitoring_bus,
                     instance_name=self._instance_name,
                     metric_domain=MetricRecordDomain.CAS) as bytes_counter:
            while bytes_remaining > 0:
                block_data = result.read(min(self.BLOCK_SIZE, bytes_remaining))
                yield bytestream_pb2.ReadResponse(data=block_data)
                bytes_counter.increment(len(block_data))
                bytes_remaining -= self.BLOCK_SIZE

    @timed_method(CAS_BYTESTREAM_WRITE_TIME_METRIC_NAME, MetricRecordDomain.CAS, instanced=True)
    @ExceptionCounter(CAS_EXCEPTION_COUNT_METRIC_NAME, metric_domain=MetricRecordDomain.CAS)
    def write(self, digest_hash, digest_size, first_block, other_blocks):
        if self.__read_only:
            raise PermissionDeniedError(
                f"ByteStream instance {self._instance_name} is read-only")

        if len(digest_hash) != HASH_LENGTH or not digest_size.isdigit():
            raise InvalidArgumentError(f"Invalid digest [{digest_hash}/{digest_size}]")

        digest = re_pb2.Digest(hash=digest_hash, size_bytes=int(digest_size))

        write_session = self.__storage.begin_write(digest)

        # Start the write session and write the first request's data.
        with Counter(metric_name=CAS_UPLOADED_BYTES_METRIC_NAME,
                     monitoring_bus=self._monitoring_bus,
                     instance_name=self._instance_name,
                     metric_domain=MetricRecordDomain.CAS) as bytes_counter:
            write_session.write(first_block)
            computed_hash = HASH(first_block)
            bytes_counter.increment(len(first_block))

            # Handle subsequent write requests.
            for next_block in other_blocks:
                write_session.write(next_block)

                computed_hash.update(next_block)
                bytes_counter.increment(len(next_block))

            # Check that the data matches the provided digest.
            if bytes_counter.count != digest.size_bytes:
                raise NotImplementedError(
                    "Cannot close stream before finishing write")

            elif computed_hash.hexdigest() != digest.hash:
                raise InvalidArgumentError("Data does not match hash")

            self.__storage.commit_write(digest, write_session)

            return bytestream_pb2.WriteResponse(committed_size=int(bytes_counter.count))
