# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from datetime import datetime
import logging
import uuid
from typing import List, Optional

from google.protobuf.duration_pb2 import Duration
from google.protobuf.timestamp_pb2 import Timestamp

from buildgrid._enums import LeaseState, OperationStage, BotStatus
from buildgrid._exceptions import CancelledError, NotFoundError
from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid._protos.google.devtools.remoteworkers.v1test2 import bots_pb2
from buildgrid._protos.google.longrunning import operations_pb2
from buildgrid._protos.google.rpc import code_pb2


class Job:

    def __init__(self, do_not_cache, action_digest, platform_requirements=None, priority=0,
                 name=None, operations=(), cancelled_operations=set(), lease=None,
                 stage=OperationStage.UNKNOWN.value, cancelled=False,
                 queued_timestamp=None, queued_time_duration=None,
                 worker_start_timestamp=None, worker_completed_timestamp=None,
                 result=None, worker_name=None, n_tries=0):
        self.__logger = logging.getLogger(__name__)

        self._name = name or str(uuid.uuid4())
        self._priority = priority
        self._lease = lease

        self.__execute_response = result
        if result is None:
            self.__execute_response = remote_execution_pb2.ExecuteResponse()
        self.__operation_metadata = remote_execution_pb2.ExecuteOperationMetadata()

        self.__queued_timestamp = Timestamp()
        if queued_timestamp is not None:
            self.__queued_timestamp.CopyFrom(queued_timestamp)

        self.__queued_time_duration = Duration()
        if queued_time_duration is not None:
            self.__queued_time_duration.CopyFrom(queued_time_duration)

        self.__worker_start_timestamp = Timestamp()
        if worker_start_timestamp is not None:
            self.__worker_start_timestamp.CopyFrom(worker_start_timestamp)

        self.__worker_completed_timestamp = Timestamp()
        if worker_completed_timestamp is not None:
            self.__worker_completed_timestamp.CopyFrom(worker_completed_timestamp)

        self.__operations_by_name = {op.name: op for op in operations}  # Name to Operation 1:1 mapping
        self.__operations_cancelled = cancelled_operations
        self.__lease_cancelled = cancelled
        self.__job_cancelled = cancelled

        self.__operation_metadata.action_digest.CopyFrom(action_digest)
        self.__operation_metadata.stage = stage

        self._do_not_cache = do_not_cache
        self._n_tries = n_tries

        self._platform_requirements = platform_requirements \
            if platform_requirements else dict()

        self.worker_name = worker_name

    def __str__(self):
        return (f"Job: name=[{self.name}], action_digest="
                f"[{self.action_digest.hash}/{self.action_digest.size_bytes}]")

    def __lt__(self, other):
        try:
            return self.priority < other.priority
        except AttributeError:
            return NotImplemented

    def __le__(self, other):
        try:
            return self.priority <= other.priority
        except AttributeError:
            return NotImplemented

    def __eq__(self, other):
        if isinstance(other, Job):
            return self.name == other.name
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __gt__(self, other):
        try:
            return self.priority > other.priority
        except AttributeError:
            return NotImplemented

    def __ge__(self, other):
        try:
            return self.priority >= other.priority
        except AttributeError:
            return NotImplemented

    # --- Public API ---

    @property
    def name(self):
        return self._name

    @property
    def cancelled(self):
        return self.__job_cancelled

    @property
    def priority(self):
        return self._priority

    def set_priority(self, new_priority, *, data_store):
        self._priority = new_priority
        data_store.update_job(self.name, {'priority': new_priority})

    @property
    def done(self):
        return self.operation_stage == OperationStage.COMPLETED

    # --- Public API: REAPI ---

    @property
    def platform_requirements(self):
        return self._platform_requirements

    @property
    def do_not_cache(self):
        return self._do_not_cache

    @property
    def action_digest(self):
        return self.__operation_metadata.action_digest

    @property
    def operation_stage(self):
        return OperationStage(self.__operation_metadata.stage)

    @property
    def action_result(self):
        if self.__execute_response is not None:
            return self.__execute_response.result
        else:
            return None

    @property
    def execute_response(self):
        return self.__execute_response

    @execute_response.setter
    def execute_response(self, response):
        self.__execute_response = response
        for operation in self.__operations_by_name.values():
            operation.response.Pack(self.__execute_response)

    @property
    def holds_cached_result(self):
        if self.__execute_response is not None:
            return self.__execute_response.cached_result
        else:
            return False

    @property
    def queued_timestamp(self) -> Timestamp:
        return self.__queued_timestamp

    @property
    def queued_timestamp_as_datetime(self) -> Optional[datetime]:
        if self.__queued_timestamp.ByteSize():
            return self.__queued_timestamp.ToDatetime()
        return None

    @property
    def queued_time_duration(self):
        return self.__queued_time_duration

    @property
    def worker_start_timestamp(self) -> Timestamp:
        return self.__worker_start_timestamp

    @property
    def worker_start_timestamp_as_datetime(self) -> Optional[datetime]:
        if self.__worker_start_timestamp.ByteSize():
            return self.__worker_start_timestamp.ToDatetime()
        return None

    @property
    def worker_completed_timestamp(self) -> Timestamp:
        return self.__worker_completed_timestamp

    @property
    def worker_completed_timestamp_as_datetime(self) -> Optional[datetime]:
        if self.__worker_completed_timestamp.ByteSize():
            return self.__worker_completed_timestamp.ToDatetime()
        return None

    def mark_worker_started(self):
        self.__worker_start_timestamp.GetCurrentTime()

    def set_action_url(self, url):
        """Generates a CAS browser URL for the job's action."""
        if url.for_message('action', self.__operation_metadata.action_digest):
            self.__execute_response.message = url.generate()

    def set_cached_result(self, action_result, data_store):
        """Allows specifying an action result from the action cache for the job.

        Note:
            This won't trigger any :class:`Operation` stage transition.

        Args:
            action_result (ActionResult): The result from cache.
        """
        self.__execute_response.result.CopyFrom(action_result)
        self.__execute_response.cached_result = True
        data_store.store_response(self)

    def n_peers(self, watch_spec):
        if watch_spec is None:
            return 0
        return sum(self.n_peers_for_operation(operation_name, watch_spec)
                   for operation_name in watch_spec.operations)

    def n_peers_for_operation(self, operation_name, watch_spec):
        if watch_spec is None:
            return 0
        return len(watch_spec.peers_for_operation(operation_name))

    def register_new_operation(self, *, data_store):
        """Subscribes to a new job's :class:`Operation` stage changes.

        Returns:
            str: The name of the subscribed :class:`Operation`.
        """
        new_operation = operations_pb2.Operation()
        # Copy state from first existing and non cancelled operation:
        for operation in self.__operations_by_name.values():
            if operation.name not in self.__operations_cancelled:
                new_operation.CopyFrom(operation)
                break

        new_operation.name = str(uuid.uuid4())

        self.__logger.debug(f"Operation created for job [{self._name}]: [{new_operation.name}]")

        self.__operations_by_name[new_operation.name] = new_operation

        data_store.create_operation(new_operation, self._name)

        self._update_operations(data_store=data_store)

        return new_operation.name

    def get_all_operations(self) -> List[operations_pb2.Operation]:
        """Gets all :class:`Operation` objects related to a job.

        Returns:
            list: A list of :class:`Operation` objects.
        """
        return [
            self.get_operation(operation_name) for operation_name in self.__operations_by_name.keys()
        ]

    def get_operation(self, operation_name):
        """Returns a copy of the the job's :class:`Operation`.

        Args:
            operation_name (str): the operation's name.

        Raises:
            NotFoundError: If no operation with `operation_name` exists.
        """
        try:
            operation = self.__operations_by_name[operation_name]

        except KeyError:
            raise NotFoundError(f"Operation name does not exist: [{operation_name}]")

        return self._get_packed_operation(operation)

    def update_operation_stage(self, stage, *, data_store):
        """Operates a stage transition for the job's :class:`Operation`.

        Args:
            stage (OperationStage): the operation stage to transition to.
        """
        if stage.value == self.__operation_metadata.stage:
            return

        changes = {}

        self.__operation_metadata.stage = stage.value
        changes["stage"] = stage.value

        self.__logger.debug(
            f"Stage changed for job [{self._name}]: [{stage.name}] (operation)")

        if self.__operation_metadata.stage == OperationStage.QUEUED.value:
            if self.__queued_timestamp.ByteSize() == 0:
                self.__queued_timestamp.GetCurrentTime()
                changes["queued_timestamp"] = self.queued_timestamp_as_datetime
            self._n_tries += 1
            changes["n_tries"] = self._n_tries

        elif self.__operation_metadata.stage == OperationStage.EXECUTING.value:
            queue_in, queue_out = self.queued_timestamp_as_datetime, datetime.utcnow()
            if queue_in:
                self.__queued_time_duration.FromTimedelta(queue_out - queue_in)
                changes["queued_time_duration"] = self.__queued_time_duration.seconds
            else:
                self.__logger.warning("Tried to calculate `queued_time_duration` "
                                      "but initial queue time wasn't set.")

        data_store.update_job(self.name, changes)

        self._update_operations(data_store=data_store)

    def cancel_all_operations(self, *, data_store):
        for operation in self.get_all_operations():
            self.cancel_operation(operation.name, data_store=data_store)

    def cancel_operation(self, operation_name, *, data_store):
        """Triggers a job's :class:`Operation` cancellation.

        This may cancel any job's :class:`Lease` that may have been issued.

        Args:
            operation_name (str): the operation's name.

        Raises:
            NotFoundError: If no operation with `operation_name` exists.
        """
        try:
            operation = self.__operations_by_name[operation_name]

        except KeyError:
            raise NotFoundError(f"Operation name does not exist: [{operation_name}]")

        self.__operations_cancelled.add(operation.name)

        self.__logger.debug(
            f"Operation cancelled for job [{self._name}]: [{operation.name}]")

        ongoing_operations = set(self.__operations_by_name.keys())
        # Job is cancelled if all the operation are:
        self.__job_cancelled = ongoing_operations.issubset(self.__operations_cancelled)

        if self.__job_cancelled:
            self.__operation_metadata.stage = OperationStage.COMPLETED.value
            changes = {
                "stage": OperationStage.COMPLETED.value,
                "cancelled": True
            }
            data_store.update_job(self.name, changes)
            if self._lease is not None:
                self.cancel_lease(data_store=data_store)

        self._update_operations(data_store=data_store)

    # --- Public API: RWAPI ---

    @property
    def lease(self):
        return self._lease

    @property
    def lease_state(self):
        if self._lease is not None:
            return LeaseState(self._lease.state)
        else:
            return None

    @property
    def lease_cancelled(self):
        return self.__lease_cancelled

    @property
    def n_tries(self):
        return self._n_tries

    def create_lease(self, worker_name, bot_id=None, *, data_store):
        """Emits a new :class:`Lease` for the job.

        Only one :class:`Lease` can be emitted for a given job. This method
        should only be used once, any further calls are ignored.

        Args:
            worker_name (string): The name of the worker this lease is for.
            bot_id (string): The name of the corresponding bot for this job's worker.
        """
        if self._lease is not None:
            return self._lease
        elif self.__job_cancelled:
            return None

        self._lease = bots_pb2.Lease()
        self._lease.id = self._name
        self._lease.payload.Pack(self.__operation_metadata.action_digest)
        self._lease.state = LeaseState.UNSPECIFIED.value

        if bot_id is None:
            bot_id = "UNKNOWN"
        self.__logger.debug(
            f"Lease created for job [{self._name}]: [{self._lease.id}] (assigned to bot [{bot_id}])")

        self.update_lease_state(LeaseState.PENDING, skip_lease_persistence=True, data_store=data_store)

        self.worker_name = worker_name

        return self._lease

    def update_lease_state(self, state, status=None, result=None,
                           skip_lease_persistence=False, *, data_store):
        """Operates a state transition for the job's current :class:`Lease`.

        Args:
            state (LeaseState): the lease state to transition to.
            status (google.rpc.Status, optional): the lease execution status,
                only required if `state` is `COMPLETED`.
            result (google.protobuf.Any, optional): the lease execution result,
                only required if `state` is `COMPLETED`.
        """
        if state.value == self._lease.state:
            return

        job_changes = {}
        lease_changes = {}

        self._lease.state = state.value
        lease_changes["state"] = state.value

        self.__logger.debug(f"State changed for job [{self._name}]: [{state.name}] (lease)")

        if self._lease.state == LeaseState.PENDING.value:
            self.__worker_start_timestamp.Clear()
            self.__worker_completed_timestamp.Clear()
            job_changes["worker_start_timestamp"] = self.worker_start_timestamp_as_datetime
            job_changes["worker_completed_timestamp"] = self.worker_completed_timestamp_as_datetime

            self._lease.status.Clear()
            self._lease.result.Clear()
            lease_changes["status"] = self._lease.status.code

        elif self._lease.state == LeaseState.COMPLETED.value:
            self.__worker_completed_timestamp.GetCurrentTime()
            job_changes["worker_completed_timestamp"] = self.worker_completed_timestamp_as_datetime

            action_result = remote_execution_pb2.ActionResult()

            # TODO: Make a distinction between build and bot failures!
            if status.code != code_pb2.OK:
                self._do_not_cache = True
                job_changes["do_not_cache"] = True

            lease_changes["status"] = status.code

            if result is not None and result.Is(action_result.DESCRIPTOR):
                result.Unpack(action_result)

            action_metadata = action_result.execution_metadata
            action_metadata.queued_timestamp.CopyFrom(self.__queued_timestamp)
            action_metadata.worker_start_timestamp.CopyFrom(self.__worker_start_timestamp)
            action_metadata.worker_completed_timestamp.CopyFrom(self.__worker_completed_timestamp)

            self.__execute_response.result.CopyFrom(action_result)
            self.__execute_response.cached_result = False
            self.__execute_response.status.CopyFrom(status)

        data_store.update_job(self.name, job_changes)
        if not skip_lease_persistence:
            data_store.update_lease(self.name, lease_changes)

    def cancel_lease(self, *, data_store):
        """Triggers a job's :class:`Lease` cancellation.

        Note:
            This will not cancel the job's :class:`Operation`.
        """
        self.__lease_cancelled = True

        self.__logger.debug(f"Lease cancelled for job [{self._name}]: [{self._lease.id}]")

        if self._lease is not None:
            self.update_lease_state(LeaseState.CANCELLED, data_store=data_store)

    def delete_lease(self):
        """Discard the job's :class:`Lease`.

        Note:
            This will not cancel the job's :class:`Operation`.
        """
        if self._lease is not None:
            self.__worker_start_timestamp.Clear()
            self.__worker_completed_timestamp.Clear()

            self.__logger.debug(f"Lease deleted for job [{self._name}]: [{self._lease.id}]")

            self._lease = None

    # --- Public API: Monitoring ---

    def query_queue_time(self):
        return self.__queued_time_duration.ToTimedelta()

    def query_n_retries(self):
        return self._n_tries - 1 if self._n_tries > 0 else 0

    # --- Private API ---

    def _get_packed_operation(self, operation: operations_pb2.Operation):
        """Return a copy of the operation with the metadata and response packed.

        Args:
            operation (:class:`buildgrid._protos.google.longrunning.operations_pb2.Operation`):
                The operation to copy.
        """
        cancelled = operation.name in self.__operations_cancelled

        # Create a copy of the operation
        new_operation = operations_pb2.Operation()
        new_operation.CopyFrom(operation)

        # Pack the metadata, setting the stage to COMPLETED if the operation
        # is cancelled.
        operation_metadata = remote_execution_pb2.ExecuteOperationMetadata()
        operation_metadata.CopyFrom(self.__operation_metadata)
        if cancelled:
            operation_metadata.stage = OperationStage.COMPLETED.value
        new_operation.metadata.Pack(operation_metadata)

        # Pack the response, setting the status code and message appropriately
        # if the operation is cancelled.
        execute_response = remote_execution_pb2.ExecuteResponse()
        execute_response.CopyFrom(self.__execute_response)
        if cancelled:
            execute_response.status.code = code_pb2.CANCELLED
            execute_response.status.message = "Operation cancelled by client."
        new_operation.response.Pack(execute_response)

        # Set whether or not the operation is done, based on the state of
        # the job and whether or not the operation is cancelled (cancelled
        # operations are always also done).
        new_operation.done = self.done or cancelled

        return new_operation

    def _update_operations(self, *, data_store):
        """Update done/cancelled state of operations in the data store."""

        for operation in self.__operations_by_name.values():
            changes = {}
            cancelled = operation.name in self.__operations_cancelled
            if cancelled:
                operation.done = True
                changes["cancelled"] = True

            elif self.done and not operation.done:
                operation.done = True

            if changes:
                data_store.update_operation(operation.name, changes)

    def get_operation_update(self, operation_name):
        """Get an operation update message tuple.

        The returned tuple is of the form

            (error, operation_state)

        """
        operation = self.__operations_by_name[operation_name]
        if operation_name in self.__operations_cancelled:
            message = (CancelledError("Operation has been cancelled"),
                       self._get_packed_operation(operation))
        else:
            message = (None, self._get_packed_operation(operation))
        return message
