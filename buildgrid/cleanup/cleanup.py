# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import asyncio
import logging
import signal

from buildgrid._enums import CleanupFailure
from buildgrid.server.cas.storage.s3 import S3Storage
from buildgrid.server.cas.storage.index.sql import SQLIndex
from buildgrid.server._monitoring import MonitoringBus, MonitoringOutputType, MonitoringOutputFormat


class CASCleanUp:
    """Creates a LRU CAS cleanup service."""

    def __init__(self, dry_run, high_watermark, low_watermark, sleep_interval, batch_size,
                 storages, indexes, monitor, mon_endpoint_type=None, mon_endpoint_location=None,
                 mon_serialisation_format=None, mon_metric_prefix=None):

        self._logger = logging.getLogger(__name__)

        self._dry_run = dry_run

        self._high_watermark = high_watermark
        self._low_watermark = low_watermark
        self._batch_size = batch_size

        self._storages = storages
        self._indexes = indexes

        self._is_instrumented = monitor

        self._sleep_interval = sleep_interval

        self._main_loop = asyncio.get_event_loop()

        if self._is_instrumented:
            self._monitoring_bus = MonitoringBus(
                self._main_loop, endpoint_type=mon_endpoint_type,
                endpoint_location=mon_endpoint_location,
                metric_prefix=mon_metric_prefix,
                serialisation_format=mon_serialisation_format)

    # --- Public API ---

    def start(self, *, on_server_start_cb=None):
        """ Start cleanup service """
        self._main_loop.add_signal_handler(signal.SIGTERM, self.stop)
        if self._is_instrumented:
            self._monitoring_bus.start()

        for instance_name in self._storages:
            if self._storages[instance_name].is_cleanup_enabled():
                if self._dry_run:
                    self._calculate_cleanup(instance_name)
                else:
                    asyncio.ensure_future(self._cleanupWorker(instance_name))
            else:
                self._logger.info(f"CleanUp for instance '{instance_name}' skipped.")
        if not self._dry_run:
            self._main_loop.run_forever()

    def stop(self):
        """ Stops the cleanup service """
        if self._is_instrumented:
            self._monitoring_bus.stop()
        if not self._dry_run:
            self._main_loop.stop()

    # --- Private API ---

    def _calculate_cleanup(self, instance_name):
        """Work out which blobs will be deleted by the cleanup command.

        Args:
            instance_name (str): The instance to do a cleanup dry-run for.

        """
        self._logger.info(f"Cleanup dry run for instance '{instance_name}'")
        index = self._indexes[instance_name]
        total_size = index.get_total_size()
        self._logger.info(
            f"CAS size is {total_size} bytes, compared with a high water mark of "
            f"{self._high_watermark} bytes and a low water mark of {self._low_watermark} bytes.")
        if total_size >= self._high_watermark:
            required_space = total_size - self._low_watermark
            digests = index.mark_n_bytes_as_deleted(required_space, self._dry_run)
            cleared_space = sum(digest.size_bytes for digest in digests)
            self._logger.info(
                f"{len(digests)} digests will be deleted, freeing up {cleared_space} bytes.")
        else:
            self._logger.info(
                f"Total size {total_size} is less than the high water mark, "
                "nothing will be deleted.")

    async def _cleanupWorker(self, instance_name):
        """ Cleanup when full """
        storage = self._storages[instance_name]
        index = self._indexes[instance_name]
        self._logger.info(f"CleanUp for instance '{instance_name}' started.")
        while True:
            total_size = index.get_total_size()
            if total_size >= self._high_watermark:
                to_delete = total_size - self._low_watermark
                self._logger.info(
                    f"CAS size for instance '{instance_name}' is {total_size} bytes, at least "
                    f"{to_delete} bytes will be cleared.")
                self._logger.info(f"Deleting items from storage/index for instance '{instance_name}'.")
                while total_size > self._low_watermark:
                    digests = index.mark_n_bytes_as_deleted(self._batch_size)
                    failed_deletions = storage.bulk_delete(digests)
                    actual_failures = []

                    if failed_deletions:
                        actual_failures = set(failure.hash for failure in failed_deletions
                                              if failure.reason is not CleanupFailure.MISSING)
                        missing = set(failure.hash for failure in failed_deletions
                                      if failure not in actual_failures)
                        if actual_failures:
                            self._logger.debug(
                                f"Failed to delete {len(actual_failures)} blobs.")
                            for failure in actual_failures:
                                self._logger.debug(
                                    f"Failed to delete {failure}.")
                        if missing:
                            self._logger.info(
                                f"{len(missing)} blobs were already missing from the storage.")

                    digests_to_delete = [digest for digest in digests
                                         if digest.hash not in actual_failures]
                    index.bulk_delete(digests_to_delete)
                    total_size = index.get_total_size()
                self._logger.info(f"Finished cleanup. CAS size is now {total_size} bytes.")
            await asyncio.sleep(self._sleep_interval)
