.. _using:

Using
=====

This section covers how to run an use the BuildGrid build service.

Basic workflow
--------------

.. toctree::
   :maxdepth: 3
    
   using_internal.rst

Client tools
------------

.. toctree::
   :maxdepth: 3

   using_bazel.rst
   using_buildstream.rst
   using_recc.rst


Alternative workers
-------------------

.. toctree::
   :maxdepth: 3

   using_buildbox.rst


Other
-----

.. toctree::
   :maxdepth: 3

   using_indexed_cas.rst
   using_cas_server.rst
   using_bb_browser.rst
