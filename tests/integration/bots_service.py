# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# pylint: disable=redefined-outer-name


import os
from datetime import datetime, timedelta
import random
from collections import Counter
import tempfile
import time
from unittest import mock

import asyncio
import grpc
from grpc._server import _Context
import pytest

from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid._protos.google.devtools.remoteworkers.v1test2 import bots_pb2
from buildgrid.server.cas.storage import lru_memory_cache
from buildgrid.server.job import LeaseState, BotStatus
from buildgrid.server.bots import service
from buildgrid.server.bots.service import BotsService
from buildgrid.server.bots.instance import BotsInterface
from buildgrid.server.persistence.mem.impl import MemoryDataStore
from buildgrid.server.persistence.sql.impl import SQLDataStore

server = mock.create_autospec(grpc.server)


# GRPC context
@pytest.fixture
def context():
    context_mock = mock.MagicMock(spec=_Context)
    context_mock.time_remaining.return_value = 10
    yield context_mock


@pytest.fixture
def bot_session():
    bot = bots_pb2.BotSession()
    bot.bot_id = 'ana'
    bot.status = BotStatus.OK.value
    yield bot


BOT_SESSION_KEEPALIVE_TIMEOUT_OPTIONS = [None, 1, 2]
DATA_STORE_IMPLS = ["sql", "mem"]

PARAMS = [(impl, timeout)
          for timeout in BOT_SESSION_KEEPALIVE_TIMEOUT_OPTIONS
          for impl in DATA_STORE_IMPLS]


@pytest.fixture(params=PARAMS)
def bots_interface(request):
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)
    impl, timeout = request.param
    if impl == "sql":
        _, db = tempfile.mkstemp()
        data_store = SQLDataStore(storage, connection_string=f"sqlite:///{db}", automigrate=True)
    elif impl == "mem":
        data_store = MemoryDataStore(storage)
    try:
        yield BotsInterface(data_store, bot_session_keepalive_timeout=timeout)
    finally:
        data_store.watcher_keep_running = False
        if impl == "sql":
            if os.path.exists(db):
                os.remove(db)


# Instance to test
@pytest.fixture
def instance(bots_interface):
    instances = {"": bots_interface}
    with mock.patch.object(service, 'bots_pb2_grpc'):
        bots_service = BotsService(server)
        for k, v in instances.items():
            bots_service.add_instance(k, v)
        yield bots_service


# Instance to test with monitoring enabled
@pytest.fixture
def instance_monitored(bots_interface):
    instances = {"": bots_interface}
    with mock.patch.object(service, 'bots_pb2_grpc'):
        bots_service = BotsService(server, monitor=True)
        for k, v in instances.items():
            bots_service.add_instance(k, v)
        yield bots_service


def _inject_work(scheduler, action=None, action_digest=None,
                 platform_requirements=None):
    if not action:
        action = remote_execution_pb2.Action()

    if not action_digest:
        action_digest = remote_execution_pb2.Digest()

    scheduler.queue_job_action(action, action_digest, platform_requirements,
                               skip_cache_lookup=True)


BOT_COUNT = [2]
HISTORY_LENGTH = [3]

BOT_SESSION_HISTORY = [(bot_count, history_len)
                       for bot_count in BOT_COUNT
                       for history_len in HISTORY_LENGTH]


@pytest.fixture(params=BOT_SESSION_HISTORY)
def update_bot_session_history(request):
    yield request.param


def check_bot_session_request_response_and_assigned_expiry(instance, request, response, request_time):
    assert isinstance(response, bots_pb2.BotSession)

    bot_session_keepalive_timeout = instance._instances[""]._bot_session_keepalive_timeout

    assert request.bot_id == response.bot_id

    if bot_session_keepalive_timeout:
        # See if expiry time was set (when enabled)
        assert response.expire_time.IsInitialized()
        # See if it was set to an expected time
        earliest_expire_time = request_time + timedelta(seconds=bot_session_keepalive_timeout)
        # We don't want to do strict comparisons because we are not looking for
        # exact time precision (e.g. in ms/ns)
        response_expire_time = response.expire_time.ToDatetime()
        assert response_expire_time >= earliest_expire_time


def test_create_bot_session(bot_session, context, instance):
    request_time = datetime.utcnow()
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)

    response = instance.CreateBotSession(request, context)

    check_bot_session_request_response_and_assigned_expiry(instance, bot_session, response, request_time)


def test_create_bot_session_bot_id_fail(context, instance):
    bot = bots_pb2.BotSession()

    request = bots_pb2.CreateBotSessionRequest(parent='',
                                               bot_session=bot)

    instance.CreateBotSession(request, context)

    context.set_code.assert_called_once_with(grpc.StatusCode.INVALID_ARGUMENT)


def test_recreate_bot_session(bot_session, context, instance):
    # This test simulates bots re-connecting due to e.g. restarting suddenly
    request_time = datetime.utcnow()
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)

    response = instance.CreateBotSession(request, context)
    initial_botsession_bot_id = response.bot_id
    initial_botsession_name = response.name

    check_bot_session_request_response_and_assigned_expiry(instance, bot_session, response, request_time)

    bots_interface = instance._get_instance("")
    with mock.patch.object(bots_interface, '_close_bot_session', autospec=True) as close_botsession_fn:
        request_time = datetime.utcnow()
        request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)

        response = instance.CreateBotSession(request, context)
        new_botsession_bot_id = response.bot_id
        new_botsession_name = response.name

        # New BotSession should be for the same BotID
        assert initial_botsession_bot_id == new_botsession_bot_id
        # New BotSession should have a new name!
        assert initial_botsession_name != new_botsession_name

        check_bot_session_request_response_and_assigned_expiry(instance, bot_session, response, request_time)

        # The previous botsession for this bot must have gotten closed
        assert close_botsession_fn.call_count == 1


def test_recreate_bot_session_as_initial_bot_session_expires(bot_session, context, instance):
    # This tests edge cases in which a botsession is re-created as it expires.
    request_time = datetime.utcnow()
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)

    response = instance.CreateBotSession(request, context)
    initial_botsession_bot_id = response.bot_id
    initial_botsession_name = response.name

    check_bot_session_request_response_and_assigned_expiry(instance, bot_session, response, request_time)

    bots_interface = instance._get_instance("")

    # Simulate simultaneous expiry of earlier botsession
    # and recreation of a new BotSession with a `CreateBotSession` request

    # Re-create botsession (e.g. bot crashed)
    # Causes a call to `_close_bot_session`
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    response = instance.CreateBotSession(request, context)
    new_botsession_bot_id = response.bot_id
    new_botsession_name = response.name

    # Simultaneously, the reaper calls tries to close the botsession
    # Force expiry
    next_botsession_expiry_override = (initial_botsession_name, request_time)
    with mock.patch.object(bots_interface, '_get_next_botsession_expiry',
                           return_value=next_botsession_expiry_override):
        bots_interface._reap_next_expired_session()

    # New BotSession should be for the same BotID
    assert initial_botsession_bot_id == new_botsession_bot_id
    # New BotSession should have a new name!
    assert initial_botsession_name != new_botsession_name


def test_recreate_bot_session_after_expiry(bot_session, context, instance):
    # Test that a bot is able to successfully create a new botsession after
    # their BotSession expires.
    request_time = datetime.utcnow()
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)

    response = instance.CreateBotSession(request, context)
    initial_botsession_bot_id = response.bot_id
    initial_botsession_name = response.name

    check_bot_session_request_response_and_assigned_expiry(instance, bot_session, response, request_time)

    bots_interface = instance._get_instance("")

    # Force expiry
    next_botsession_expiry_override = (initial_botsession_name, request_time)
    with mock.patch.object(bots_interface, '_get_next_botsession_expiry',
                           return_value=next_botsession_expiry_override):
        bots_interface._reap_next_expired_session()

    # Bot returns and creates a new session
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    response = instance.CreateBotSession(request, context)
    new_botsession_bot_id = response.bot_id
    new_botsession_name = response.name

    # New BotSession should be for the same BotID
    assert initial_botsession_bot_id == new_botsession_bot_id
    # New BotSession should have a new name!
    assert initial_botsession_name != new_botsession_name


def test_update_bot_session_zombie(bot_session, context, instance):
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = instance.CreateBotSession(request, context)
    # Update server with incorrect UUID by rotating it
    bot.name = bot.name[len(bot.name): 0]

    request = bots_pb2.UpdateBotSessionRequest(name=bot.name,
                                               bot_session=bot)

    instance.UpdateBotSession(request, context)

    context.set_code.assert_called_once_with(grpc.StatusCode.INVALID_ARGUMENT)


def test_update_bot_session_bot_id_fail(bot_session, context, instance):
    request = bots_pb2.UpdateBotSessionRequest(bot_session=bot_session)

    instance.UpdateBotSession(request, context)

    context.set_code.assert_called_once_with(grpc.StatusCode.INVALID_ARGUMENT)


def test_update_bot_session_after_expiry(bot_session, context, instance):
    # Test that a bot is able to successfully create a new botsession after
    # their BotSession expires.
    request_time = datetime.utcnow()
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)

    response = instance.CreateBotSession(request, context)
    initial_botsession = response
    initial_botsession_name = response.name

    check_bot_session_request_response_and_assigned_expiry(instance, bot_session, response, request_time)

    bots_interface = instance._get_instance("")

    # Close bot session (e.g. expiry)
    bots_interface._close_bot_session(initial_botsession_name, reason='expired')

    # Bot now sends an `UpdateBotSession` request
    request = bots_pb2.UpdateBotSessionRequest(name=initial_botsession_name, bot_session=initial_botsession)
    response = instance.UpdateBotSession(request, context)

    # The response should contain the relevant error code
    context.set_code.assert_called_once_with(grpc.StatusCode.DATA_LOSS)
    # The botsession name of the response shouldn't match the initial name
    assert response.name != initial_botsession_name
    # The botsession name of the response should be None
    assert response.name == ''


def test_update_bot_session(bot_session, context, instance):
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = instance.CreateBotSession(request, context)

    request_time = datetime.utcnow()
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name,
                                               bot_session=bot)

    response = instance.UpdateBotSession(request, context)

    check_bot_session_request_response_and_assigned_expiry(instance, bot_session, response, request_time)


@pytest.mark.parametrize("sleep_duration", [1, 2])
def test_update_bot_session_and_wait_for_expiry(bot_session, context, instance, sleep_duration):
    bots_interface = instance._get_instance("")
    bot_session_keepalive_timeout = bots_interface._bot_session_keepalive_timeout

    with mock.patch.object(bots_interface, '_close_bot_session', autospec=True) as close_botsession_fn:
        request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
        bot = instance.CreateBotSession(request, context)

        request_time = datetime.utcnow()
        request = bots_pb2.UpdateBotSessionRequest(name=bot.name,
                                                   bot_session=bot)

        response = instance.UpdateBotSession(request, context)

        check_bot_session_request_response_and_assigned_expiry(instance, bot_session, response, request_time)

        if bot_session_keepalive_timeout:
            assert bots_interface._next_expire_time_occurs_in() >= 0

        time.sleep(sleep_duration)
        # Call this manually since the asyncio event loop isn't running
        bots_interface._reap_next_expired_session()

        if bot_session_keepalive_timeout and bot_session_keepalive_timeout <= sleep_duration:
            # the BotSession should have expired after sleeping `sleep_duration`
            assert close_botsession_fn.call_count == 1
            # Should have no tracked expiries now
        else:
            # no timeout, or timeout > 1, shouldn't see any expiries yet.
            assert close_botsession_fn.call_count == 0


@pytest.mark.parametrize("bot_session_keepalive_timeout", BOT_SESSION_KEEPALIVE_TIMEOUT_OPTIONS)
def test_bots_instance_sets_up_reaper_loop(bot_session_keepalive_timeout):
    main_loop = asyncio.get_event_loop()
    with mock.patch.object(main_loop, 'create_task', autospec=True) as loop_create_task_fn:
        with mock.patch.object(BotsInterface, '_reap_expired_sessions_loop', autospec=True):
            # Just instantiate and see whether the __init__ sets up the reaper loop when needed
            my_instance = BotsInterface(
                None, bot_session_keepalive_timeout=bot_session_keepalive_timeout)

            # If the timeout was set, the reaper task should have been created, otherwise it shouldn't
            if bot_session_keepalive_timeout:
                assert loop_create_task_fn.call_count == 1
            else:
                assert loop_create_task_fn.call_count == 0


@pytest.mark.parametrize("sleep_duration", [1, 2])
def test_botsession_expiry_cleanup(bot_session, context, instance, sleep_duration):
    bots_interface = instance._get_instance("")
    bot_session_keepalive_timeout = bots_interface._bot_session_keepalive_timeout

    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = instance.CreateBotSession(request, context)

    request_time = datetime.utcnow()
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name,
                                               bot_session=bot)

    response = instance.UpdateBotSession(request, context)

    check_bot_session_request_response_and_assigned_expiry(instance, bot_session, response, request_time)

    if bot_session_keepalive_timeout:
        assert len(bots_interface._ordered_expire_times_by_botsession) == 1
        assert bots_interface._next_expire_time_occurs_in() >= 0

    time.sleep(sleep_duration)
    # Call this manually since the asyncio event loop isn't running
    bots_interface._reap_next_expired_session()

    if bot_session_keepalive_timeout and bot_session_keepalive_timeout <= sleep_duration:
        # Should have no tracked expiries now
        assert len(bots_interface._ordered_expire_times_by_botsession) == 0
        # Next expiry should be None
        assert bots_interface._next_expire_time_occurs_in() is None


@pytest.mark.parametrize("number_of_jobs", [0, 1, 3, 500])
def test_number_of_leases(number_of_jobs, bot_session, context, instance):

    for _ in range(0, number_of_jobs):
        _inject_work(instance._instances[""]._scheduler)

    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    response = instance.CreateBotSession(request, context)

    assert len(response.leases) == min(number_of_jobs, 1)


def test_createbotsession_leases_with_work(bot_session, context, instance):
    request = bots_pb2.CreateBotSessionRequest(parent='',
                                               bot_session=bot_session)

    action_digest = remote_execution_pb2.Digest(hash='gaff')
    _inject_work(instance._instances[""]._scheduler, action_digest=action_digest)

    response = instance.CreateBotSession(request, context)

    assert len(response.leases) == 1
    response_action = remote_execution_pb2.Digest()
    response.leases[0].payload.Unpack(response_action)

    assert isinstance(response, bots_pb2.BotSession)
    assert response.leases[0].state == LeaseState.PENDING.value
    assert response_action == action_digest


def test_updatebotsession_leases_with_work(bot_session, context, instance):
    # CreateBotSession
    request = bots_pb2.CreateBotSessionRequest(parent='',
                                               bot_session=bot_session)

    response = instance.CreateBotSession(request, context)
    assert len(response.leases) == 0

    # Work queued on the server
    action_digest = remote_execution_pb2.Digest(hash='gaff')
    _inject_work(instance._instances[""]._scheduler, action_digest=action_digest)

    bot_session = response  # the server has assigned a session name
    # UpdateBotSession should receive a lease
    request = bots_pb2.UpdateBotSessionRequest(name=bot_session.name,
                                               bot_session=bot_session)

    response = instance.UpdateBotSession(request, context)
    assert len(response.leases) == 1
    response_action = remote_execution_pb2.Digest()
    response.leases[0].payload.Unpack(response_action)

    assert isinstance(response, bots_pb2.BotSession)
    assert response.leases[0].state == LeaseState.PENDING.value
    assert response_action == action_digest


def test_lease_reassignment_after_botsession_close(bot_session, context, instance):
    bots_interface = instance._get_instance("")
    request = bots_pb2.CreateBotSessionRequest(parent='', bot_session=bot_session)

    action_digest = remote_execution_pb2.Digest(hash='gaff')
    _inject_work(bots_interface._scheduler, action_digest=action_digest)

    response = instance.CreateBotSession(request, context)

    assert len(response.leases) == 1
    response_action = remote_execution_pb2.Digest()
    response.leases[0].payload.Unpack(response_action)

    assert isinstance(response, bots_pb2.BotSession)
    assert response.leases[0].state == LeaseState.PENDING.value
    assert response_action == action_digest

    assert len(bots_interface._assigned_leases) == 1

    # Invoke `_close_bot_session`
    bots_interface._close_bot_session(response.name, reason='test')
    # Check whether the lease assignment has been cleaned up
    assert len(bots_interface._assigned_leases) == 0

    request = bots_pb2.CreateBotSessionRequest(parent='', bot_session=bot_session)
    # Check whether a new botsession is able to pick up the re-queued lease
    response = instance.CreateBotSession(request, context)

    assert len(response.leases) == 1
    response_action = remote_execution_pb2.Digest()
    response.leases[0].payload.Unpack(response_action)

    assert isinstance(response, bots_pb2.BotSession)
    assert response.leases[0].state == LeaseState.PENDING.value
    assert response_action == action_digest

    assert len(bots_interface._assigned_leases) == 1


def test_bot_disconnects_as_lease_is_being_assigned(bot_session, context, instance):
    bots_interface = instance._get_instance("")
    request = bots_pb2.CreateBotSessionRequest(parent='', bot_session=bot_session)

    # Bot connects
    response = instance.CreateBotSession(request, context)
    assert len(response.leases) == 0

    # Work is queued
    action_digest = remote_execution_pb2.Digest(hash='gaff')
    _inject_work(bots_interface._scheduler, action_digest=action_digest)

    # Bot disconnects
    bots_interface._close_bot_session(response.name, reason='test')

    bot_session = response
    # Simultaneously, `UpdateBotSession` is waiting on leases
    # (patch checking functionality to simulate simultaneous events) and
    # waiting on leases from scheduler
    with mock.patch.object(bots_interface, '_check_bot_ids', autospec=True):
        with mock.patch.object(bots_interface, '_check_assigned_leases', autospec=True):
            with mock.patch.object(bots_interface, '_bot_ids', autospec=True):
                request = bots_pb2.UpdateBotSessionRequest(name=bot_session.name,
                                                           bot_session=bot_session)
                # The BotSession no longer exists due to the `_close_bot_session`, this shouldn't
                # cause problems to buildgrid though, it should let it resume normally
                response = instance.UpdateBotSession(request, context)

    # Make sure the lease is now assigned to another bot!
    request = bots_pb2.CreateBotSessionRequest(parent='', bot_session=bot_session)
    # Check whether a new botsession is able to pick up the re-queued lease
    response = instance.CreateBotSession(request, context)

    assert len(response.leases) == 1
    response_action = remote_execution_pb2.Digest()
    response.leases[0].payload.Unpack(response_action)

    assert isinstance(response, bots_pb2.BotSession)
    assert response.leases[0].state == LeaseState.PENDING.value
    assert response_action == action_digest

    assert len(bots_interface._assigned_leases) == 1


def test_post_bot_event_temp(context, instance):
    request = bots_pb2.PostBotEventTempRequest()
    instance.PostBotEventTemp(request, context)

    context.set_code.assert_called_once_with(grpc.StatusCode.UNIMPLEMENTED)


def test_unmet_platform_requirements(bot_session, context, instance):
    request = bots_pb2.CreateBotSessionRequest(parent='',
                                               bot_session=bot_session)

    action_digest = remote_execution_pb2.Digest(hash='gaff')
    _inject_work(instance._instances[""]._scheduler,
                 action_digest=action_digest,
                 platform_requirements={'OSFamily': set('wonderful-os')})

    response = instance.CreateBotSession(request, context)

    assert len(response.leases) == 0


def test_unhealthy_bot(bot_session, context, instance):
    # set botstatus to unhealthy
    bot_session.status = BotStatus.UNHEALTHY.value
    request = bots_pb2.CreateBotSessionRequest(parent='',
                                               bot_session=bot_session)

    action_digest = remote_execution_pb2.Digest(hash='gaff')
    _inject_work(instance._instances[""]._scheduler, action_digest=action_digest)

    response = instance.CreateBotSession(request, context)

    # No leases should be given
    assert len(response.leases) == 0


def _create_bot_session(name, instance, context):
    bot = bots_pb2.BotSession()
    bot.status = BotStatus.OK.value
    bot.bot_id = name

    request = bots_pb2.CreateBotSessionRequest(parent='', bot_session=bot)
    new_bot_session = instance.CreateBotSession(request, context)

    return new_bot_session


def test_update_bot_session_state_reflected_in_metrics(update_bot_session_history,
                                                       context, instance_monitored):
    TRACKED_BOT_STATUSES = (BotStatus.OK, BotStatus.UNHEALTHY, BotStatus.BOT_TERMINATING)

    n_bots, history_len = update_bot_session_history
    # Generate history of desired length
    bots_history = []
    for i in range(history_len):
        bot_statuses = [random.choice(TRACKED_BOT_STATUSES) for i in range(n_bots)]
        # Calculate # of bots in each status
        total_bots_by_status = Counter()
        total_bots_by_status.update(bot_statuses)
        bots_history.append((bot_statuses, total_bots_by_status))

    # Create all the bot sessions
    bot_sessions = []
    for i in range(n_bots):
        bot_id = f"bot_{i}"
        new_bot_session = _create_bot_session(bot_id, instance_monitored, context)
        print(f"bot id=[{bot_id}] has name=[{new_bot_session.name}]")
        bot_sessions.append(new_bot_session)

    process_order = [i for i in range(n_bots)]
    # Simulate UpdateBotSession calls from all bots and check metrics
    for iteration in range(history_len):
        print(f"Iteration: {iteration}")
        # Update all BotSessions (in random order for each iteraion)
        random.shuffle(process_order)  # Shuffle update order
        status_updates, total_bots_by_status = bots_history[iteration]
        for bot_index in process_order:
            current_bot_session = bot_sessions[bot_index]
            assert (f"bot_{bot_index}") == current_bot_session.bot_id
            # Update status
            current_bot_session.status = status_updates[bot_index].value
            request = bots_pb2.UpdateBotSessionRequest(name=current_bot_session.name,
                                                       bot_session=current_bot_session)
            update_bot_session_response = instance_monitored.UpdateBotSession(request, context)
            assert (f"bot_{bot_index}") == update_bot_session_response.bot_id

        print("Bot statuses:")
        print([bot.status for bot in bot_sessions])

        # Make sure the total number of bots matches expected number
        assert n_bots == instance_monitored.query_n_bots()

        # Make sure the numbers of the tracked states match up
        for status in TRACKED_BOT_STATUSES:
            # Print this so that it is visible in case of errors
            print(f"Asserting expected numbers for status: {status} (value={status.value})")
            assert total_bots_by_status[status] == instance_monitored.query_n_bots_for_status(status)
