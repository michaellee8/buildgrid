# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# pylint: disable=redefined-outer-name


from datetime import datetime, timedelta
import io
from unittest import mock

import grpc
from grpc._server import _Context
import pytest

from buildgrid._enums import MetricRecordDomain
from buildgrid._protos.google.bytestream import bytestream_pb2
from buildgrid._protos.google.rpc import code_pb2
from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2 as re_pb2
from buildgrid._protos.buildgrid.v2.monitoring_pb2 import MetricRecord
from buildgrid.server.cas.storage.storage_abc import StorageABC
from buildgrid.server.cas.instance import ByteStreamInstance, ContentAddressableStorageInstance
from buildgrid.server.cas import service
from buildgrid.server.cas.service import ByteStreamService, ContentAddressableStorageService
from buildgrid.server.metrics_names import (
    CAS_EXCEPTION_COUNT_METRIC_NAME,
    CAS_DOWNLOADED_BYTES_METRIC_NAME,
    CAS_UPLOADED_BYTES_METRIC_NAME,
    CAS_FIND_MISSING_BLOBS_NUM_REQUESTED_METRIC_NAME,
    CAS_FIND_MISSING_BLOBS_NUM_MISSING_METRIC_NAME,
    CAS_FIND_MISSING_BLOBS_PERCENT_MISSING_METRIC_NAME)
from buildgrid.settings import HASH
from buildgrid.utils import create_digest

from tests.utils.metrics import mock_create_timer_record, mock_create_counter_record, mock_create_distribution_record


context = mock.create_autospec(_Context)
server = mock.create_autospec(grpc.server)


class SimpleStorage(StorageABC):
    """Storage provider wrapper around a dictionary.

    Does not attempt to delete old entries, so this is only useful for testing.
    """

    def __init__(self, existing_data=None):
        self.data = {}
        self.map_data = existing_data
        if existing_data:
            for datum in existing_data:
                self.data[(HASH(datum).hexdigest(), len(datum))] = datum

    def has_blob(self, digest):
        return (digest.hash, digest.size_bytes) in self.data

    def get_blob(self, digest):
        key = (digest.hash, digest.size_bytes)
        return io.BytesIO(self.data[key]) if key in self.data else None

    def delete_blob(self, digest):
        key = (digest.hash, digest.size_bytes)
        self.data.pop(key, None)

    def begin_write(self, digest):
        result = io.BytesIO()
        result.digest = digest
        return result

    def commit_write(self, digest, write_session):
        assert write_session.digest == digest
        data = write_session.getvalue()
        assert HASH(data).hexdigest() == digest.hash
        assert len(data) == digest.size_bytes
        self.data[(digest.hash, digest.size_bytes)] = data

    def get_message(self, digest, message_type):
        datum = self.data[(digest.hash, digest.size_bytes)]
        message = re_pb2.Directory()
        message.directories.extend(self.map_data[datum])
        return message


test_strings = [b"", b"hij"]
instances = ["", "test_inst"]


def setup_bytestream_service(storage, instance_name, monitoring_bus, *, read_only=False):
    bs_instance = ByteStreamInstance(storage, read_only=read_only)
    # Have to set the instance name manually since normally
    # it's done by calling register_instance_with_server
    bs_instance._instance_name = instance_name

    bs_service = ByteStreamService(server)
    bs_service.add_instance(instance_name, bs_instance)

    if monitoring_bus:
        bs_instance.add_monitoring_bus(monitoring_bus)
        bs_service.add_monitoring_bus(monitoring_bus)

    return bs_service


def setup_cas_service(storage, instance_name, monitoring_bus, *, read_only=False):
    cas_instance = ContentAddressableStorageInstance(storage, read_only=read_only)
    # Have to set the instance name manually since normally
    # it's done by calling register_instance_with_server
    cas_instance._instance_name = instance_name

    cas_service = ContentAddressableStorageService(server)
    cas_service.add_instance(instance_name, cas_instance)

    if monitoring_bus:
        cas_instance.add_monitoring_bus(monitoring_bus)
        cas_service.add_monitoring_bus(monitoring_bus)

    return cas_service


def test_empty_blob_is_pre_inserted_for_bytestream():
    storage = SimpleStorage([])

    empty_digest = create_digest(b'')
    assert not storage.has_blob(empty_digest)

    ByteStreamInstance(storage, read_only=False)
    assert storage.has_blob(empty_digest)


@pytest.mark.parametrize("monitored", [True, False])
@pytest.mark.parametrize("data_to_read", test_strings)
@pytest.mark.parametrize("instance_name", instances)
@mock.patch.object(service, 'bytestream_pb2_grpc', autospec=True)
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
@mock.patch('buildgrid.server.metrics_utils.create_counter_record', new=mock_create_counter_record)
def test_bytestream_read(mocked, data_to_read, instance_name, monitored):
    storage = SimpleStorage([b"abc", b"defg", data_to_read])

    monitoring_bus = None
    if monitored:
        monitoring_bus = mock.Mock()

    bs_service = setup_bytestream_service(storage, instance_name, monitoring_bus)

    request = bytestream_pb2.ReadRequest()
    if instance_name != "":
        request.resource_name = instance_name + "/"
    request.resource_name += f"blobs/{HASH(data_to_read).hexdigest()}/{len(data_to_read)}"

    data = b""
    for response in bs_service.Read(request, context):
        data += response.data
    assert data == data_to_read

    if monitored:
        service_record = mock_create_timer_record(MetricRecordDomain.CAS, "bytestream-read")
        instance_record = mock_create_timer_record(MetricRecordDomain.CAS, "bytestream-read",
                                                   metadata={'instance-name': instance_name})
        bytes_record = mock_create_counter_record(
            domain=MetricRecordDomain.CAS, name=CAS_DOWNLOADED_BYTES_METRIC_NAME,
            count=len(data_to_read), metadata={"instance-name": instance_name})
        # Most of the CAS and ByteStream service methods simply call the instance methods, so usually the
        # order of the timers' completion is instance method followed by service method. In this case,
        # however, the ByteStream read service method yields from the instance method, so its timer finishes
        # first
        call_list = [mock.call(service_record), mock.call(instance_record)]
        if len(data_to_read) > 0:
            call_list.append(mock.call(bytes_record))
        monitoring_bus.send_record_nowait.assert_has_calls(call_list)


@pytest.mark.parametrize("instance", instances)
@mock.patch.object(service, 'bytestream_pb2_grpc', autospec=True)
def test_bytestream_read_many(mocked, instance):
    context.reset_mock()
    data_to_read = b"testing" * 10000

    storage = SimpleStorage([b"abc", b"defg", data_to_read])

    bs_instance = ByteStreamInstance(storage)
    servicer = ByteStreamService(server)
    servicer.add_instance(instance, bs_instance)

    request = bytestream_pb2.ReadRequest()
    resource_source = ""
    if instance != "":
        resource_source = instance + "/"
    request.resource_name = resource_source + f"blobs/{HASH(data_to_read).hexdigest()}/{len(data_to_read)}"

    data = b""
    for response in servicer.Read(request, context):
        data += response.data
    assert data == data_to_read

    # if we try and read a bad digest we should get an invalid argument error
    bad_request = bytestream_pb2.ReadRequest()
    bad_request.resource_name = resource_source + "blobs/nothashed/2"
    for response in servicer.Read(bad_request, context):
        data += response.data

    context.set_code.assert_called_once_with(grpc.StatusCode.INVALID_ARGUMENT)


@pytest.mark.parametrize("monitored", [True, False])
@pytest.mark.parametrize("instance_name", instances)
@pytest.mark.parametrize("extra_data", ["", "/", "/extra/data"])
@mock.patch.object(service, 'bytestream_pb2_grpc', autospec=True)
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
@mock.patch('buildgrid.server.metrics_utils.create_counter_record', new=mock_create_counter_record)
def test_bytestream_write(mocked, instance_name, monitored, extra_data):
    context.reset_mock()
    storage = SimpleStorage()

    monitoring_bus = None
    if monitored:
        monitoring_bus = mock.Mock()

    bs_service = setup_bytestream_service(storage, instance_name, monitoring_bus)

    resource_source = ""
    if instance_name != "":
        resource_source = instance_name + "/"
    hash_ = HASH(b'abcdef').hexdigest()
    resource_name = resource_source + f"uploads/UUID-HERE/blobs/{hash_}/6"
    resource_name += extra_data
    requests = [
        bytestream_pb2.WriteRequest(resource_name=resource_name, data=b'abc'),
        bytestream_pb2.WriteRequest(
            data=b'def', write_offset=3, finish_write=True)
    ]

    response = bs_service.Write(iter(requests), context)
    assert response.committed_size == 6
    assert len(storage.data) == 2
    assert (hash_, 6) in storage.data
    assert storage.data[(hash_, 6)] == b'abcdef'

    # Now do the same request again without hashing and we should get an exception
    # because the hash will be invalid
    resource_name = resource_source + "uploads/UUID-HERE/blobs/nothashed/6"
    resource_name += extra_data

    bs_service.Write(iter([bytestream_pb2.WriteRequest(resource_name=resource_name, data=b'abc')]),
                     context)
    context.set_code.assert_called_once_with(grpc.StatusCode.INVALID_ARGUMENT)
    if monitored:
        metadata = {'instance-name': instance_name}
        service_record = mock_create_timer_record(MetricRecordDomain.CAS, "bytestream-write")
        instance_record = mock_create_timer_record(MetricRecordDomain.CAS,
                                                   "bytestream-write", metadata={'instance-name': instance_name})
        exception_record = mock_create_counter_record(MetricRecordDomain.CAS,
                                                      CAS_EXCEPTION_COUNT_METRIC_NAME, metadata=metadata)
        bytes_record = mock_create_counter_record(
            domain=MetricRecordDomain.CAS, name=CAS_UPLOADED_BYTES_METRIC_NAME,
            count=6, metadata=metadata)
        # We call the method twice, but the second call throws an exception in the instance that's
        # handled by the service, so only the service method generates a metric the second time
        call_list = [mock.call(bytes_record), mock.call(service_record), mock.call(instance_record),
                     mock.call(service_record), mock.call(exception_record)]

        monitoring_bus.send_record_nowait.assert_has_calls(call_list, any_order=True)


@mock.patch.object(service, 'bytestream_pb2_grpc', autospec=True)
def test_bytestream_write_rejects_wrong_hash(mocked):
    context.reset_mock()
    storage = SimpleStorage()

    bs_instance = ByteStreamInstance(storage)
    servicer = ByteStreamService(server)
    servicer.add_instance("", bs_instance)

    data = b'some data'
    wrong_hash = HASH(b'incorrect').hexdigest()
    resource_name = f"uploads/UUID-HERE/blobs/{wrong_hash}/9"
    requests = [
        bytestream_pb2.WriteRequest(
            resource_name=resource_name, data=data, finish_write=True)
    ]

    servicer.Write(iter(requests), context)
    context.set_code.assert_called_once_with(grpc.StatusCode.INVALID_ARGUMENT)
    context.set_code.reset_mock()

    wrong_digest = re_pb2.Digest(hash=wrong_hash, size_bytes=len(data))
    assert not storage.has_blob(wrong_digest)


@mock.patch.object(service, 'bytestream_pb2_grpc', autospec=True)
def test_bytestream_read_only_write_fails(mocked):
    storage = SimpleStorage()

    bs_instance = ByteStreamInstance(storage, read_only=True)
    servicer = ByteStreamService(server)
    servicer.add_instance("", bs_instance)

    data = b'some data'
    hash_ = HASH(b'some data').hexdigest()
    resource_name = f"uploads/UUID-HERE/blobs/{hash_}/9"
    requests = [
        bytestream_pb2.WriteRequest(
            resource_name=resource_name, data=data, finish_write=True)
    ]

    servicer.Write(iter(requests), context)
    context.set_code.assert_called_once_with(grpc.StatusCode.PERMISSION_DENIED)
    context.set_code.reset_mock()

    digest = re_pb2.Digest(hash=hash_, size_bytes=len(data))
    assert not storage.has_blob(digest)


def test_empty_blob_is_pre_inserted_for_cas_service():
    storage = SimpleStorage([])

    empty_digest = create_digest(b'')
    assert not storage.has_blob(empty_digest)

    ContentAddressableStorageInstance(storage, read_only=False)
    assert storage.has_blob(empty_digest)


@pytest.mark.parametrize("monitored", [True, False])
@pytest.mark.parametrize("instance_name", instances)
@mock.patch.object(service, 'remote_execution_pb2_grpc', autospec=True)
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
@mock.patch('buildgrid.server.metrics_utils.create_distribution_record', new=mock_create_distribution_record)
def test_cas_find_missing_blobs(mocked, instance_name, monitored):
    storage = SimpleStorage([b'abc', b'def'])

    monitoring_bus = None
    if monitored:
        monitoring_bus = mock.Mock()

    cas_service = setup_cas_service(storage, instance_name, monitoring_bus)

    digests = [
        re_pb2.Digest(hash=HASH(b'def').hexdigest(), size_bytes=3),
        re_pb2.Digest(hash=HASH(b'def').hexdigest(), size_bytes=3),  # duplicate request
        re_pb2.Digest(hash=HASH(b'ghij').hexdigest(), size_bytes=4)
    ]
    request = re_pb2.FindMissingBlobsRequest(
        instance_name=instance_name, blob_digests=digests)
    response = cas_service.FindMissingBlobs(request, context)
    assert len(response.missing_blob_digests) == 1
    assert response.missing_blob_digests[0] == digests[2]

    if monitored:
        service_record = mock_create_timer_record(MetricRecordDomain.CAS, "find-missing-blobs")
        instance_record = mock_create_timer_record(MetricRecordDomain.CAS, "find-missing-blobs",
                                                   metadata={'instance-name': instance_name})

        metadata = {'instance-name': instance_name} if instance_name else None
        num_requested = mock_create_distribution_record(MetricRecordDomain.CAS,
                                                        CAS_FIND_MISSING_BLOBS_NUM_REQUESTED_METRIC_NAME,
                                                        2.0, metadata)
        num_missing = mock_create_distribution_record(MetricRecordDomain.CAS,
                                                      CAS_FIND_MISSING_BLOBS_NUM_MISSING_METRIC_NAME,
                                                      1.0, metadata)
        percent_missing = mock_create_distribution_record(MetricRecordDomain.CAS,
                                                          CAS_FIND_MISSING_BLOBS_PERCENT_MISSING_METRIC_NAME,
                                                          50.00, metadata)

        call_list = [mock.call(num_requested),
                     mock.call(num_missing), mock.call(percent_missing),
                     mock.call(instance_record), mock.call(service_record)]
        monitoring_bus.send_record_nowait.assert_has_calls(call_list)


@pytest.mark.parametrize("monitored", [True, False])
@pytest.mark.parametrize("instance_name", instances)
@mock.patch.object(service, 'remote_execution_pb2_grpc', autospec=True)
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
@mock.patch('buildgrid.server.metrics_utils.create_counter_record', new=mock_create_counter_record)
def test_cas_batch_update_blobs(mocked, instance_name, monitored):
    storage = SimpleStorage()

    monitoring_bus = None
    if monitored:
        monitoring_bus = mock.Mock()

    cas_service = setup_cas_service(storage, instance_name, monitoring_bus)

    update_requests = [
        re_pb2.BatchUpdateBlobsRequest.Request(
            digest=re_pb2.Digest(hash=HASH(b'abc').hexdigest(), size_bytes=3), data=b'abc'),
        re_pb2.BatchUpdateBlobsRequest.Request(  # duplicate request
            digest=re_pb2.Digest(hash=HASH(b'abc').hexdigest(), size_bytes=3), data=b'abc'),
        re_pb2.BatchUpdateBlobsRequest.Request(
            digest=re_pb2.Digest(hash="invalid digest!", size_bytes=1000),
            data=b'wrong data')
    ]

    request = re_pb2.BatchUpdateBlobsRequest(
        instance_name=instance_name, requests=update_requests)

    response = cas_service.BatchUpdateBlobs(request, context)

    assert len(response.responses) == 2

    for blob_response in response.responses:
        if blob_response.digest == update_requests[0].digest:
            assert blob_response.status.code == 0

        elif blob_response.digest == update_requests[2].digest:
            assert blob_response.status.code != 0

        else:
            raise Exception("Unexpected blob response")

    assert len(storage.data) == 2
    assert (update_requests[0].digest.hash, 3) in storage.data
    assert storage.data[(update_requests[0].digest.hash, 3)] == b'abc'

    if monitored:
        metadata = {'instance-name': instance_name}
        service_record = mock_create_timer_record(MetricRecordDomain.CAS, "batch-update-blobs")
        instance_record = mock_create_timer_record(MetricRecordDomain.CAS, "batch-update-blobs",
                                                   metadata=metadata)
        bytes_record = mock_create_counter_record(
            domain=MetricRecordDomain.CAS, name=CAS_UPLOADED_BYTES_METRIC_NAME,
            count=3, metadata=metadata)
        call_list = [mock.call(bytes_record), mock.call(instance_record), mock.call(service_record)]
        monitoring_bus.send_record_nowait.assert_has_calls(call_list)


@pytest.mark.parametrize("instance", instances)
@mock.patch.object(service, 'remote_execution_pb2_grpc', autospec=True)
def test_cas_read_only_update_blobs(mocked, instance):
    storage = SimpleStorage()

    cas_instance = ContentAddressableStorageInstance(storage, read_only=True)
    servicer = ContentAddressableStorageService(server)
    servicer.add_instance(instance, cas_instance)

    update_requests = [
        re_pb2.BatchUpdateBlobsRequest.Request(
            digest=re_pb2.Digest(hash=HASH(b'abc').hexdigest(), size_bytes=3), data=b'abc'),
        re_pb2.BatchUpdateBlobsRequest.Request(
            digest=re_pb2.Digest(hash="invalid digest!", size_bytes=1000),
            data=b'wrong data')
    ]

    request = re_pb2.BatchUpdateBlobsRequest(
        instance_name=instance, requests=update_requests)
    servicer.BatchUpdateBlobs(request, context)

    context.set_code.assert_called_once_with(grpc.StatusCode.PERMISSION_DENIED)
    context.set_code.reset_mock()

    assert not storage.has_blob(update_requests[0].digest)
    assert not storage.has_blob(update_requests[1].digest)


@pytest.mark.parametrize("monitored", [True, False])
@pytest.mark.parametrize("instance_name", instances)
@mock.patch.object(service, 'remote_execution_pb2_grpc', autospec=True)
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
@mock.patch('buildgrid.server.metrics_utils.create_counter_record', new=mock_create_counter_record)
def test_cas_batch_read_blobs(mocked, instance_name, monitored):
    data = {b'abc', b'defg', b'hij', b'klmnop', b'duplicate blob'}
    data_hashes = {HASH(blob).hexdigest() for blob in data}
    storage = SimpleStorage(data)

    monitoring_bus = None
    if monitored:
        monitoring_bus = mock.Mock()

    cas_service = setup_cas_service(storage, instance_name, monitoring_bus)

    bloblists_to_request = [
        [b'abc', b'defg'],
        [b'defg', b'missing_blob'],
        [b'missing_blob'],
        [b'duplicate blob', b'duplicate blob']
    ]

    digest_lists = [
        [
            re_pb2.Digest(hash=HASH(blob).hexdigest(), size_bytes=len(blob))
            for blob in bloblist
        ]
        for bloblist in bloblists_to_request
    ]

    read_requests = [
        re_pb2.BatchReadBlobsRequest(
            instance_name=instance_name, digests=digest_list
        )
        for digest_list in digest_lists
    ]

    for request, bloblist in zip(read_requests, bloblists_to_request):
        batched_responses = cas_service.BatchReadBlobs(request, context)
        response_blobs = set()

        for response in batched_responses.responses:
            if response.digest.hash in data_hashes:
                assert response.status.code == code_pb2.OK
                response_blobs.add(response.data)
            else:
                assert response.status.code == code_pb2.NOT_FOUND

        assert response_blobs == set(bloblist) & data

        if monitored:
            service_record = mock_create_timer_record(MetricRecordDomain.CAS, "batch-read-blobs")
            instance_record = mock_create_timer_record(MetricRecordDomain.CAS, "batch-read-blobs",
                                                       metadata={'instance-name': instance_name})
            expected_bytes = sum(len(blob) for blob in set(bloblist) if blob in data)
            bytes_record = mock_create_counter_record(
                domain=MetricRecordDomain.CAS, name=CAS_DOWNLOADED_BYTES_METRIC_NAME,
                count=expected_bytes, metadata={'instance-name': instance_name})
            call_list = [mock.call(bytes_record), mock.call(instance_record), mock.call(service_record)]
            monitoring_bus.send_record_nowait.assert_has_calls(call_list)
            monitoring_bus.reset_mock()


@pytest.mark.parametrize("instance", instances)
@mock.patch.object(service, 'remote_execution_pb2_grpc', autospec=True)
def test_cas_batch_bad_digest(mocked, instance):
    data = {b'abc', b'def'}
    # 'aaa' is an invalid hash and should be rejected.
    data_hashes = {HASH(blob).hexdigest() for blob in data}
    data_hashes.add('aaa')

    storage = SimpleStorage(data)

    cas_instance = ContentAddressableStorageInstance(storage)
    servicer = ContentAddressableStorageService(server)
    servicer.add_instance(instance, cas_instance)
    read_request = re_pb2.BatchReadBlobsRequest(
        instance_name=instance, digests=[
            re_pb2.Digest(hash=h,
                          size_bytes=3) for h in data_hashes])

    # Batch Read
    batched_responses = servicer.BatchReadBlobs(read_request, context)
    for response in batched_responses.responses:
        if response.digest.hash == 'aaa':
            assert response.status.code == code_pb2.INVALID_ARGUMENT
        else:
            assert response.status.code == code_pb2.OK

    # Batch update
    update_request = re_pb2.BatchUpdateBlobsRequest(
        instance_name=instance, requests=[
            re_pb2.BatchUpdateBlobsRequest.Request(
                digest=re_pb2.Digest(hash=HASH(b'foo').hexdigest(), size_bytes=3), data=b'foo'),
            re_pb2.BatchUpdateBlobsRequest.Request(
                digest=re_pb2.Digest(hash="notadigest", size_bytes=3), data=b'abc')])
    response_list = servicer.BatchUpdateBlobs(update_request, context)
    for response in response_list.responses:
        if response.digest.hash == "notadigest":
            assert response.status.code == code_pb2.INVALID_ARGUMENT
        else:
            assert response.status.code == code_pb2.OK


@pytest.mark.parametrize("monitored", [True, False])
@pytest.mark.parametrize("instance_name", instances)
@mock.patch.object(service, 'remote_execution_pb2_grpc', autospec=True)
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
@mock.patch('buildgrid.server.metrics_utils.create_counter_record', new=mock_create_counter_record)
def test_cas_get_tree(mocked, instance_name, monitored):
    '''Directory Structure:
        |--root
           |--subEmptyDir
           |--subParentDir
              |--subChildDir
    '''
    root = re_pb2.Digest(hash=HASH(b'abc').hexdigest(), size_bytes=3)
    digest1 = re_pb2.Digest(hash=HASH(b'def').hexdigest(), size_bytes=3)
    subEmptyDir = re_pb2.DirectoryNode(name=b'def', digest=digest1)
    digest2 = re_pb2.Digest(hash=HASH(b'ghi').hexdigest(), size_bytes=3)
    subParentDir = re_pb2.DirectoryNode(name=b'ghi', digest=digest2)
    digest3 = re_pb2.Digest(hash=HASH(b'xyz').hexdigest(), size_bytes=3)
    subChildDir = re_pb2.DirectoryNode(name=b'xyz', digest=digest3)

    storage = SimpleStorage({b'abc': [subEmptyDir, subParentDir], b'def': [],
                             b'ghi': [subChildDir], b'xyz': []})

    monitoring_bus = None
    if monitored:
        monitoring_bus = mock.Mock()

    cas_service = setup_cas_service(storage, instance_name, monitoring_bus)

    request = re_pb2.GetTreeRequest(
        instance_name=instance_name, root_digest=root)
    result = []
    for response in cas_service.GetTree(request, context):
        result.extend(response.directories)

    expectedRoot = re_pb2.Directory()
    expectedRoot.directories.extend([subEmptyDir, subParentDir])
    expectedEmpty = re_pb2.Directory()
    expectedParent = re_pb2.Directory()
    expectedParent.directories.extend([subChildDir])
    expectedChild = re_pb2.Directory()

    expected = [expectedRoot, expectedEmpty, expectedParent, expectedChild]
    assert result == expected

    if monitored:
        service_record = mock_create_timer_record(MetricRecordDomain.CAS, "get-tree")
        instance_record = mock_create_timer_record(MetricRecordDomain.CAS, "get-tree",
                                                   metadata={'instance-name': instance_name})
        bytes_record = mock_create_counter_record(
            domain=MetricRecordDomain.CAS, name=CAS_DOWNLOADED_BYTES_METRIC_NAME,
            count=12, metadata={"instance-name": instance_name})
        # Most of the CAS service methods simply call the instance methods, so usually the order of the
        # timers' completion is instance method followed by service method. In this case, however, the
        # get_tree service method yields from the instance method, so its timer finishes first
        call_list = [mock.call(service_record), mock.call(instance_record), mock.call(bytes_record)]
        monitoring_bus.send_record_nowait.assert_has_calls(call_list)
