#!/bin/bash
# This script returns a random value to use as the OSFamily

os[0]="FakeOS_0"
os[1]="FakeOS_1"
os[2]="FakeOS_2"

len=${#os[@]}
rand=$(($RANDOM % $len))
echo ${os[$rand]}

