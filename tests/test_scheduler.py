# Copyright (C) 2019 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# pylint: disable=redefined-outer-name


import copy
import os
import tempfile
import time
from unittest import mock

import grpc
from grpc._server import _Context
import pytest

from buildgrid._enums import LeaseState, OperationStage
from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2

from buildgrid.utils import create_digest
from buildgrid.server.controller import ExecutionController
from buildgrid.server.cas.storage import lru_memory_cache
from buildgrid.server.actioncache.instance import ActionCache
from buildgrid.server.execution import service
from buildgrid.server.execution.service import ExecutionService
from buildgrid.server.persistence.mem.impl import MemoryDataStore
from buildgrid.server.persistence.sql.impl import SQLDataStore


server = mock.create_autospec(grpc.server)

command = remote_execution_pb2.Command()
command_digest = create_digest(command.SerializeToString())

action = remote_execution_pb2.Action(command_digest=command_digest,
                                     do_not_cache=True)
action_digest = create_digest(action.SerializeToString())


@pytest.fixture
def context():
    cxt = mock.MagicMock(spec=_Context)
    yield cxt


PARAMS = [(impl, use_cache) for impl in ["sql", "mem"]
          for use_cache in ["action-cache", "no-action-cache"]]


# Return informative test ids for tests using the controller fixture
def idfn(params_value):
    return f"{params_value[0]}-{params_value[1]}"


@pytest.fixture(params=PARAMS, ids=idfn)
def controller(request):
    impl, use_cache = request.param
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)

    write_session = storage.begin_write(command_digest)
    write_session.write(command.SerializeToString())
    storage.commit_write(command_digest, write_session)

    write_session = storage.begin_write(action_digest)
    write_session.write(action.SerializeToString())
    storage.commit_write(action_digest, write_session)

    if impl == "sql":
        _, db = tempfile.mkstemp()
        data_store = SQLDataStore(storage, connection_string=f"sqlite:///{db}", automigrate=True)
    elif impl == "mem":
        data_store = MemoryDataStore(storage)
    try:
        if use_cache == "action-cache":
            cache = ActionCache(storage, 50)
            yield ExecutionController(
                data_store, storage=storage, action_cache=cache, property_keys=['OSFamily', 'ISA'])
        else:
            yield ExecutionController(data_store, storage=storage, property_keys=['OSFamily', 'ISA'])
    finally:
        data_store.watcher_keep_running = False
        if impl == "sql":
            if os.path.exists(db):
                os.remove(db)


PARAMS_MAX_EXECUTION = [(impl, max_execution_timeout) for impl in ["sql", "mem"]
                        for max_execution_timeout in [None, 0.1, 2]]


@pytest.fixture(params=PARAMS_MAX_EXECUTION, ids=idfn)
def controller_max_execution_timeout(request):
    impl, max_execution_timeout = request.param
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)

    write_session = storage.begin_write(command_digest)
    write_session.write(command.SerializeToString())
    storage.commit_write(command_digest, write_session)

    write_session = storage.begin_write(action_digest)
    write_session.write(action.SerializeToString())
    storage.commit_write(action_digest, write_session)

    if impl == "sql":
        _, db = tempfile.mkstemp()
        data_store = SQLDataStore(storage, connection_string=f"sqlite:///{db}", automigrate=True)
    elif impl == "mem":
        data_store = MemoryDataStore(storage)
    try:
        yield ExecutionController(data_store, storage=storage,
                                  property_keys=['OSFamily', 'ISA'],
                                  max_execution_timeout=max_execution_timeout)
    finally:
        if impl == "sql":
            data_store.watcher_keep_running = False
            if os.path.exists(db):
                os.remove(db)


def test_unregister_operation_peer(controller, context):
    scheduler = controller.execution_instance._scheduler
    job_name = scheduler.queue_job_action(action, action_digest, skip_cache_lookup=True)

    operation_name = controller.execution_instance.register_job_peer(job_name,
                                                                     context.peer())

    watch_spec = scheduler.data_store.watched_jobs.get(job_name)
    assert watch_spec is not None
    assert operation_name in watch_spec.operations
    assert context.peer() in watch_spec.peers_for_operation(operation_name)

    controller.execution_instance.unregister_operation_peer(operation_name, context.peer())
    watch_spec = scheduler.data_store.watched_jobs.get(job_name)
    assert watch_spec is None

    operation_name = controller.execution_instance.register_job_peer(job_name,
                                                                     context.peer())
    scheduler._update_job_operation_stage(job_name, OperationStage.COMPLETED)
    controller.execution_instance.unregister_operation_peer(operation_name, context.peer())
    if isinstance(scheduler.data_store, MemoryDataStore):
        assert scheduler.data_store.get_job_by_name(job_name) is None
    elif isinstance(scheduler.data_store, SQLDataStore):
        assert job_name not in scheduler.data_store.response_cache


@pytest.mark.parametrize("monitoring", [True, False])
def test_update_lease_state(controller, context, monitoring):
    scheduler = controller.execution_instance._scheduler
    with mock.patch.object(scheduler.data_store, 'activate_monitoring', autospec=True) as activate_monitoring_fn:
        if monitoring:
            scheduler.activate_monitoring()
            assert activate_monitoring_fn.call_count == 1
        else:
            assert activate_monitoring_fn.call_count == 0

    job_name = scheduler.queue_job_action(action, action_digest, skip_cache_lookup=True)

    job = scheduler.data_store.get_job_by_name(job_name)
    job_lease = job.create_lease("test-suite", data_store=scheduler.data_store)
    if isinstance(scheduler.data_store, SQLDataStore):
        scheduler.data_store.create_lease(job_lease)

    lease = copy.deepcopy(job_lease)
    scheduler.update_job_lease_state(job_name, lease)

    lease.state = LeaseState.ACTIVE.value
    scheduler.update_job_lease_state(job_name, lease)
    job = scheduler.data_store.get_job_by_name(job_name)
    assert lease.state == job._lease.state

    lease.state = LeaseState.COMPLETED.value
    scheduler.update_job_lease_state(job_name, lease)
    job = scheduler.data_store.get_job_by_name(job_name)
    if not isinstance(scheduler.data_store, SQLDataStore):
        assert lease.state == job._lease.state
    else:
        assert job._lease is None

    with mock.patch.object(scheduler.data_store, 'deactivate_monitoring', autospec=True) as deactivate_monitoring_fn:
        if monitoring:
            # TODO: Actually test that monitoring functioned as expected
            scheduler.deactivate_monitoring()
            assert deactivate_monitoring_fn.call_count == 1
        else:
            assert deactivate_monitoring_fn.call_count == 0


def test_retry_job_lease(controller, context):
    scheduler = controller.execution_instance._scheduler
    scheduler.MAX_N_TRIES = 2

    job_name = scheduler.queue_job_action(action, action_digest, skip_cache_lookup=True)
    scheduler._update_job_operation_stage(job_name, OperationStage.EXECUTING)

    job = scheduler.data_store.get_job_by_name(job_name)

    job_lease = job.create_lease("test-suite", data_store=scheduler.data_store)
    if isinstance(scheduler.data_store, SQLDataStore):
        scheduler.data_store.create_lease(job_lease)

    scheduler.retry_job_lease(job_name)

    job = scheduler.data_store.get_job_by_name(job_name)
    assert job.n_tries == 2

    scheduler.retry_job_lease(job_name)

    job = scheduler.data_store.get_job_by_name(job_name)
    assert job.n_tries == 2
    assert job.operation_stage == OperationStage.COMPLETED


def test_requeue_queued_job(controller, context):
    scheduler = controller.execution_instance._scheduler

    job_name = scheduler.queue_job_action(action, action_digest, skip_cache_lookup=True)

    job = scheduler.data_store.get_job_by_name(job_name)

    job_lease = job.create_lease("test-suite", data_store=scheduler.data_store)
    if isinstance(scheduler.data_store, SQLDataStore):
        scheduler.data_store.create_lease(job_lease)

    leases = scheduler.request_job_leases({})
    assert len(leases) == 1

    job = scheduler.data_store.get_job_by_name(job_name)
    assert job.lease in leases
    assert job.operation_stage == OperationStage.EXECUTING

    # Make sure that retrying a job that was assigned but
    # not marked as in progress properly re-queues

    scheduler.retry_job_lease(job_name)
    job = scheduler.data_store.get_job_by_name(job_name)
    assert job.operation_stage == OperationStage.QUEUED

    leases = scheduler.request_job_leases({})
    assert len(leases) == 1

    job = scheduler.data_store.get_job_by_name(job_name)
    assert job.lease in leases
    assert job.operation_stage == OperationStage.EXECUTING


def test_get_all_operations(controller, context):
    """ Test that the scheduler reports the correct
    number of operations when calling get_all_operations() """
    scheduler = controller.execution_instance._scheduler
    assert len(scheduler.get_all_operations()) == 0
    job_name = scheduler.queue_job_action(action, action_digest, skip_cache_lookup=True)
    assert len(scheduler.get_all_operations()) == 0
    scheduler.register_job_peer(job_name, "bot1")
    scheduler.register_job_peer(job_name, "bot2")
    scheduler.register_job_peer(job_name, "bot3")
    assert len(scheduler.get_all_operations()) == 3


@pytest.mark.parametrize("n_operations", [1, 2])
@pytest.mark.parametrize("sleep_time", [0.2])
@pytest.mark.parametrize("trigger_request",
                         [None, 'get_job_operation', 'register_job_peer',
                          'register_job_operation_peer', 'queue_job'])
def test_max_execution_timeout(controller_max_execution_timeout, context,
                               sleep_time, n_operations, trigger_request):
    scheduler = controller_max_execution_timeout.execution_instance._scheduler

    data_store = scheduler.data_store
    max_execution_timeout = scheduler._max_execution_timeout

    # Queue Job
    job_name = scheduler.queue_job_action(action, action_digest, skip_cache_lookup=True)
    job = data_store.get_job_by_name(job_name)

    # Create n operations
    operation_names = []
    for i in range(n_operations):
        operation_names.append(job.register_new_operation(data_store=data_store))

    for i in range(n_operations):
        operation_initially = scheduler.get_job_operation(operation_names[i])
        assert operation_initially.done is False

    # Mark this job as "executing"
    job_lease = job.create_lease('test-worker', 'bot-id', data_store=data_store)

    def _assign_lease_cb(job):
        job.mark_worker_started()
        return [job_lease]
    data_store.assign_lease_for_next_job({}, _assign_lease_cb)  # Sets the start time
    if isinstance(scheduler.data_store, SQLDataStore):
        scheduler.data_store.create_lease(job_lease)

    job_lease.state = LeaseState.ACTIVE.value
    scheduler.update_job_lease_state(job_name, job_lease)

    # Make sure it was marked as "Executing"
    job_after_assignment = data_store.get_job_by_name(job_name)
    assert job_after_assignment.operation_stage is OperationStage.EXECUTING

    # Wait and see it marked as cancelled when exceeding execution timeout...
    time.sleep(sleep_time)

    # Simulate a request (or not) to see if the timeout has kicked in
    if trigger_request:
        if trigger_request == 'get_job_operation':
            _ = scheduler.get_job_operation(operation_names[0])
        elif trigger_request == 'register_job_peer':
            scheduler.register_job_peer(job_name, 'test-peer')
        elif trigger_request == 'register_job_operation_peer':
            _ = scheduler.register_job_operation_peer(operation_names[0], 'test-peer-2')
        elif trigger_request == 'queue_job':
            _ = scheduler.queue_job_action(action, action_digest)

    # Retrieve job from data store after the request that could've triggered expiry
    job_after_sleep = data_store.get_job_by_name(job_name)

    if trigger_request and max_execution_timeout and sleep_time >= max_execution_timeout:
        for operation in job_after_sleep.get_all_operations():
            assert operation.done is True
        assert job_after_sleep.cancelled is True
    else:
        for operation in job_after_sleep.get_all_operations():
            assert operation.done is False
        assert job_after_sleep.cancelled is False
