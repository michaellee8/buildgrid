coverage==5.0.3
moto==1.3.14
pep8==1.7.1
psutil==5.6.7
pytest==5.3.2
pytest-cov==2.8.1
pytest-pep8==1.0.6
pytest-pylint==0.14.1
pytest-xdist==1.31.0
fakeredis==1.1.0
redis==3.3.11 # Due to an issue with 3.4.0 (https://github.com/andymccurdy/redis-py/issues/1276), and redis being a dependency of fakeredis.
testing.postgresql==1.3.0
psycopg2-binary==2.8.4
